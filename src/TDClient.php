<?php

namespace MiamiOH\TeamDynamix;

use Carbon\Carbon;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\ClientException;
use Psr\Http\Client\ClientInterface;
use Psr\Http\Message\RequestInterface;
use Psr\Http\Message\ResponseInterface;

/**
 * Class TDClient
 * @package App\MiamiOH\TeamDynamix
 */
class TDClient
{
    /**
     * @var Client
     */
    private $client;
    /**
     * @var string
     */
    private $token = null;
    /**
     * @var string
     */
    private $password;
    /**
     * @var string|null
     */
    private $username;
    /**
     * @var string
     */
    private $appid;

    /**
     * @var bool
     */
    private $notifyRequestor;

    /**
     * @return string
     */
    public function getAppId(): string
    {
        return $this->appid;
    }

    /**
     * @param string $appId
     * @param Client $client
     * @param string $username
     * @param string $password
     * @param bool $notifyRequestor
     */
    public function __construct(
        string $appId,
        Client $client,
        string $username,
        string $password,
        bool $notifyRequestor = true
    ) {
        $this->client = $client;
        $this->appid = $appId;
        $this->username = $username;
        $this->password = $password;
        $this->notifyRequestor = $notifyRequestor;
    }

    /**
     * @param string $path
     * @return ResponseInterface
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function get(string $path): ResponseInterface
    {
        // need to authenticate and have valid token before request is made
        $headers = [
            'Content-Type' => 'application/json',
            'Authorization' => 'Bearer ' . $this->getToken(),
        ];
        try {
            $response = $this->client->get($path, [
                'headers' => $headers,
            ]);
            return $response;
        } catch (ClientException $e) {
            $response = $e->getResponse();
            if ($response->getStatusCode() == 429) {
                $end_date = Carbon::parse($response->getHeader('X-RateLimit-Reset')[0]);
                $sleepTime = $end_date->diffInRealSeconds(Carbon::now()) + 1;
                //                Log::info('Encountered 429 Error with GET, waiting '. $sleepTime .' seconds, until ' .$end_date);
                sleep($sleepTime);
                return $this->get($path);
            } else {
                throw $e;
            }
        }
    }

    /**
     * @return string
     */
    public function getToken(): string
    {
        if (!empty($this->token)) {
            return $this->token;
        };
        $response = $this->client->post(
            'auth',
            [
                'headers' => ['Content-Type' => 'application/json'],
                'body' => \json_encode([
                    'username' => $this->username,
                    'password' => $this->password,
                ]),
            ]
        );
        $this->token = $response->getBody()->getContents();
        return $this->token;
    }

    /**
     * @param string $path
     * @param array $body
     * @param array $headers
     * @return ResponseInterface
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function post(string $path, array $body, array $headers)
    {
        // need to authenticate and have valid token before request is made
        $headers['Authorization'] = 'Bearer ' . $this->getToken();
        $req = [
            'headers' => $headers,
            array_keys($body)[0] => $body[array_keys($body)[0]]
        ];
        $path = $this->appid . '/' . $path;
        if (!$this->getNotifyRequestor()) {
            $path .= '?NotifyRequestor=false';
        }
        return $this->client->post($path, $req);
    }

    /**
     * @param bool $notify
     */
    public function setNotifyRequestor(bool $notify): void
    {
        $this->notifyRequestor = $notify;
    }

    /**
     * @return bool
     */
    public function getNotifyRequestor(): bool
    {
        return $this->notifyRequestor;
    }
}
