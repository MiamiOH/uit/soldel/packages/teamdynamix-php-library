<?php

namespace MiamiOH\TeamDynamix;

use Illuminate\Support\ServiceProvider as BaseServiceProvider;

class ServiceProvider extends BaseServiceProvider
{
    private $configPath = __DIR__ . '/../config/miamioh-tdx.php';


    public function boot()
    {
        $this->publishes([
            $this->configPath => config_path('miamioh-tdx.php'),
        ]);
    }

    public function register()
    {
        $this->mergeConfigFrom($this->configPath, 'miamioh-tdx');

        /**
         * Creation of TD Classes
         */

        $tdClient = new TDClient(
            config('miamioh-tdx.appId'),
            new \GuzzleHttp\Client(['base_uri' => config('miamioh-tdx.host', '')]),
            config('miamioh-tdx.username', ''),
            config('miamioh-tdx.password', ''),
        );

        $this->app->instance(TDClient::class, $tdClient);
    }
}
