<?php

namespace MiamiOH\TeamDynamix\Api;

use MiamiOH\TeamDynamix\Collections\UserCollection;
use MiamiOH\TeamDynamix\Models\TDUser;

class Users extends BaseApi
{
    public function searchUsers(string $user, int $maxResults = 50): UserCollection
    {
        $users = $this->get("people/lookup?searchText=" . $user . "&MaxResults=" . $maxResults, false);
        $userCollection = new UserCollection();
        if (count($users) > 0) {
            foreach ($users as $user) {
                $userCollection->add(TDUser::createFromArray($user));
            }
        }
        return $userCollection;
    }
}
