<?php

namespace MiamiOH\TeamDynamix\Api;

use MiamiOH\TeamDynamix\TDClient;

abstract class BaseApi
{
    protected $client;

    public function __construct($client)
    {
        $this->client = $client;
    }

    protected function get(string $path, bool $useAppId = true)
    {
        if ($useAppId) {
            $path = $this->client->getAppId() . '/' . $path;
        }
        $response = $this->client->get($path);
        $body = $response->getBody()->getContents();
        return \json_decode($body, true);
    }

    protected function post(string $path, array $body = [], array $headers = [])
    {
        $response = $this->client->post($path, $body, $headers);
        $body = $response->getBody()->getContents();
        return \json_decode($body, true);
    }
}
