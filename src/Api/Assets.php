<?php

namespace MiamiOH\TeamDynamix\Api;

use Illuminate\Support\Collection;
use MiamiOH\TeamDynamix\Exceptions\InvalidAssetDataException;
use MiamiOH\TeamDynamix\Models\TDArticle;
use MiamiOH\TeamDynamix\Models\TDAsset;

class Assets extends BaseApi
{
    /**
     * @param string $assetId
     * @return TDAsset
     * @throws InvalidAssetDataException
     */
    public function getAsset(string $assetId): TDAsset
    {
        try {
            return TDAsset::createFromArray($this->get('assets/' . $assetId));
        } catch (\Exception $e) {
            throw new InvalidAssetDataException("Error getting TDAsset info from API response:" . $e->getMessage());
        }
    }

    public function searchForAssets(string $searchId, string $searchString = '', int $pageSize = 10, int $page = 0): Collection
    {
        $assetCollection = new Collection();
        $formParams = [
            'Page.PageIndex' => $page,
            'Page.PageSize' => $pageSize,
            'searchText' => $searchString,
        ];
        $header = [
            'Content-Type' => 'application/x-www-form-urlencoded'
        ];
        $body = [
            'form_params' => $formParams
        ];
        $response = $this->client->post('assets/searches/' . $searchId . '/results', $body, $header);
        $body = $response->getBody()->getContents();
        $body = \json_decode($body, true);

        $data = $body['Data'];
        if (count($data)) {
            foreach ($data as $assetData) {
                $asset = $this->getAsset($assetData['ID']);
                $assetCollection->add($asset);
            }
        }
        if ($body['PageSize'] * ($body['CurrentPageIndex'] + 1) < $body['TotalCount']) {
            $assetCollection = $assetCollection->merge(self::searchForAssets($searchId, $searchString, $pageSize, $page + 1));
        }

        return $assetCollection;
    }
    public function getAssetsBySearchId(string $searchId, int $pageSize = 10, int $page = 0): Collection
    {
        return $this->searchForAssets($searchId, '', $pageSize, $page);
    }

    public function getRelatedArticles(string $assetId): Collection
    {
        $articleCollection = new Collection();
        $data = $this->get('assets/' . $assetId . '/articles');
        if (count($data)) {
            foreach ($data as $articleData) {
                $article = TDArticle::createFromArray($articleData);
                $articleCollection->add($article);
            }
        }
        return $articleCollection;
    }

    public function searchReportForAssets(string $reportId, string $searchString = ''): Collection
    {
        $assetCollection = new Collection();
        $data = $this->get('reports/' . $reportId .'?withData=true', false);
        if (count($data['DataRows'])) {
            foreach ($data['DataRows'] as $assetData) {
                if (str_contains(strtolower($assetData['Name']), strtolower($searchString))) {
                    $asset = $this->getAsset($assetData['AssetID']);
                    $assetCollection->add($asset);
                }
            }
        }
        return $assetCollection;

    }
}
