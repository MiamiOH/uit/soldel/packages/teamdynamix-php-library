<?php

namespace MiamiOH\TeamDynamix\Api;

use Illuminate\Http\UploadedFile;
use MiamiOH\TeamDynamix\Collections\TicketCollection;
use MiamiOH\TeamDynamix\Exceptions\AddAttachmentException;
use MiamiOH\TeamDynamix\Exceptions\InvalidTicketDataException;
use MiamiOH\TeamDynamix\Exceptions\TicketCreationException;
use MiamiOH\TeamDynamix\Models\TDTicket;
use MiamiOH\TeamDynamix\Models\TDUser;
use MiamiOH\TeamDynamix\Collections\AssetCollection;
use MiamiOH\TeamDynamix\Models\TDAsset;

/**
 * Class Tickets
 * @package MiamiOH\TeamDynamix\Api
 */
class Tickets extends BaseApi
{
    /**
     * @param string $ticketId
     * @return TDTicket
     * @throws InvalidTicketDataException
     */
    public function getTicket(string $ticketId): TDTicket
    {
        try {
            return TDTicket::createFromArray($this->get('tickets/' . $ticketId));
        } catch (\Exception $e) {
            throw new InvalidTicketDataException("Error getting TDTicket from API response: " . $e->getMessage());
        }
    }

    /**
     * @param string $ticketId
     * @return TDTicket
     * @throws InvalidTicketDataException
     */
    public function getTicketWithAssets(string $ticketId): TDTicket
    {
        try {
            $ticket = $this->getTicket($ticketId);
            $assets = $this->get('tickets/' . $ticketId . '/assets');
            $assetCollection = new AssetCollection();
            foreach ($assets as $asset) {
                $assetCollection->add(TDAsset::createFromArray($asset));
            }
            $ticket->addAssets($assetCollection);
            return $ticket;
        } catch (\Exception $e) {
            throw new InvalidTicketDataException("Error getting TDTicket from API response: " . $e->getMessage());
        }
    }

    /**
     * @param TDUser $user
     * @return TicketCollection
     * @throws InvalidTicketDataException
     */
    public function getTicketsByUser(TDUser $user): TicketCollection
    {
        $userID = $user->getUid();
        //statusClassIDS: 1 = New, 2 = In Process, 5 = On Hold
        //to only retrieve open tickets
        $tickets = $this->post(
            'tickets/search',
            [
                "body" => json_encode([
                    "RequestorUids" => [$userID],
                    "StatusClassIDs" => explode(',', env('TDX_OPEN_STATUS_CLASS_IDS', '1,2,5'))
                ])
            ],
            [
                'Content-Type' => 'application/json'
            ]
        );
        $ticketCollection = new TicketCollection();
        if (count($tickets) > 0) {
            //if the user id is invalid or otherwise incorrect, it will just return every ticket in the app for some reason :)))))
            //so loop through the returned tickets to be sure that we're actually returning tickets created by this person
            foreach ($tickets as $ticket) {
                if ($ticket['RequestorUid'] == $userID) {
                    $ticketCollection->add(TDTicket::createFromArray($ticket));
                }
            }
        }

        return $ticketCollection;
    }

    /**
     * @param array $ticketInfo
     * @return TDTicket
     * @throws TicketCreationException
     */
    public function createTicket(array $ticketInfo): TDTicket
    {
        $headers = [
            'Content-Type' => 'application/json'
        ];
        $body = [
            'body' => json_encode($ticketInfo)
        ];
        $created = $this->post('tickets', $body, $headers);

        if ($created['ID'] == -1) {
            throw new TicketCreationException("Error creating ticket: " . $created['Message']);
        } else {
            try {
                $ticket = TDTicket::createFromArray($created);
            } catch (\Exception $e) {
                throw new TicketCreationException("Error creating TDTicket from API response: " . $e->getMessage());
            }
        }
        return $ticket;
    }

    /**
     * @param array $file
     * @param int $ticketId
     * @return array
     * @throws AddAttachmentException
     */
    public function addAttachmentToTicket(array $file, int $ticketId): array
    {
        $added = $this->post('tickets/'.$ticketId.'/attachments', $file);
        if ($added['ID'] == -1) {
            throw new AddAttachmentException("Error adding attachment to ticket " . $ticketId . " Message: " . $added['Message']);
        }
        return $added;
    }
}
