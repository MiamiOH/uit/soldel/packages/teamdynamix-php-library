<?php
/**
 * Created by PhpStorm.
 * User: tepeds
 * Date: 2019-03-01
 * Time: 15:17
 */

namespace MiamiOH\TeamDynamix\Exceptions;

class InvalidAssetDataException extends \Exception
{
}
