<?php

namespace MiamiOH\TeamDynamix\Models;

use MiamiOH\TeamDynamix\Models\BaseModel;

class TDArticle extends BaseModel
{
    /**
     * @var string
     */
    private $title;
    /**
     * @var string
     */
    private $id;
    /**
     * @var string
     */
    private $category;
    /**
     * @var bool
     */
    private $published;
    /**
     * @var string
     */
    private $appID;
    /**
     * @var string
     */
    private $baseURL;


    public function __construct(string $title, string $appID, string $id, string $category, bool $published, $baseURL = null)
    {
        $this->title = $title;
        $this->id = $id;
        $this->category = $category;
        $this->published = $published;
        $this->appID = $appID;
        if ($baseURL !== null) {
            $this->baseURL = $baseURL;
        } else {
            $this->baseURL = 'https://miamioh.teamdynamix.com/TDClient/';
        }
    }


    public static function createFromArray(array $data): TDArticle
    {
        return new TDArticle(
            $data['Subject'],
            $data['AppID'],
            $data['ID'],
            $data['CategoryName'],
            $data['IsPublished'],
            array_key_exists('baseUrl', $data) ? $data['baseUrl'] : null
        );
    }

    public function getTitle(): string
    {
        return $this->title;
    }

    public function getAppID(): string
    {
        return $this->appID;
    }

    public function getId(): string
    {
        return $this->id;
    }


    public function getCategory(): string
    {
        return $this->category;
    }

    public function isPublished(): bool
    {
        return $this->published;
    }


    public function toArray(): array
    {
        // TODO: Implement toArray() method.
    }

    public function getArticleURL(): string
    {
        return $this->baseURL.$this->getAppID() . '/Portal/KB/ArticleDet?ID='. $this->getId();

    }
}
