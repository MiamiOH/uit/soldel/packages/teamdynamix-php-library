<?php

namespace MiamiOH\TeamDynamix\Models;

use MiamiOH\TeamDynamix\Exceptions\InvalidUserDataException;

/**
 * Class TDUser
 * @package MiamiOH\TeamDynamix\Models
 */
class TDUser extends BaseModel
{
    /**
     * @var string
     */
    private $uid;
    /**
     * @var string
     */
    private $firstName;
    /**
     * @var string
     */
    private $lastName;
    /**
     * @var string
     */
    private $primaryEmail;

    /**
     * @param string $uid
     * @param string $firstName
     * @param string $lastName
     * @param string $primaryEmail
     */
    public function __construct(string $uid, string $firstName, string $lastName, string $primaryEmail)
    {
        $this->uid = $uid;
        $this->firstName = $firstName;
        $this->lastName = $lastName;
        $this->primaryEmail = $primaryEmail;
    }

    /**
     * @param array $data
     * @return TDUser
     * @throws InvalidUserDataException
     */
    public static function createFromArray(array $data): TDUser
    {
        self::validateData($data);
        return new TDUser(
            $data['UID'],
            $data['FirstName'],
            $data['LastName'],
            $data['PrimaryEmail'],
        );
    }

    /**
     * @param string $body
     * @return TDUser
     * @throws InvalidUserDataException
     */
    public static function createFromJson(string $body): TDUser
    {
        $data = json_decode($body, true);
        return self::createFromArray($data);
    }

    /**
     * @param array $data
     * @throws InvalidUserDataException
     */
    private static function validateData(array $data): void
    {
        if (!isset($data['UID'])) {
            throw new InvalidUserDataException("Error: User data has no 'UID'");
        }
    }

    /**
     * @return string
     */
    public function getUid(): string
    {
        return $this->uid;
    }

    /**
     * @return string
     */
    public function getFirstName(): string
    {
        return $this->firstName;
    }

    /**
     * @return string
     */
    public function getLastName(): string
    {
        return $this->lastName;
    }

    /**
     * @return string
     */
    public function getPrimaryEmail(): string
    {
        return $this->primaryEmail;
    }

    /**
     * @return array
     */
    public function toArray(): array
    {
        return [
            'UID' => $this->getUid(),
            'FirstName' => $this->getFirstName(),
            'LastName' => $this->getLastName(),
            'PrimaryEmail' => $this->getPrimaryEmail()
        ];
    }
}
