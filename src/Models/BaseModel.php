<?php

namespace MiamiOH\TeamDynamix\Models;

use Illuminate\Support\Collection;

abstract class BaseModel
{
    abstract public function toArray(): array;

    public function toJson(): string
    {
        return json_encode($this->toArray());
    }

    /**
     * @param Collection $attributes
     * @param string $field
     * @param string $name
     * @param string $returnField
     * @return mixed|null
     */
    protected static function extractFromAttributes(
        Collection $attributes,
        string $field,
        string $name = 'Name',
        string $returnField = 'Value'
    ) {
        foreach ($attributes as $attribute) {
            if ($attribute[$name] == $field) {
                return $attribute[$returnField];
            }
        }
        return '';
    }
}
