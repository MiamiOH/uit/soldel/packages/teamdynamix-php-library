<?php

namespace MiamiOH\TeamDynamix\Models;

use Carbon\Carbon;
use MiamiOH\TeamDynamix\Exceptions\InvalidTicketDataException;
use MiamiOH\TeamDynamix\Collections\AssetCollection;

/**
 * Class TDTicket
 * @package MiamiOH\TeamDynamix\Models
 */
class TDTicket extends BaseModel
{
    /**
     * @var int
     */
    private $id;
    /**
     * @var int
     */
    private $typeID;
    /**
     * @var string
     */
    private $title;
    /**
     * @var int
     */
    private $accountID;
    /**
     * @var int
     */
    private $statusID;
    /**
     * @var string
     */
    private $statusName;
    /**
     * @var int
     */
    private $priorityID;
    /**
     * @var string
     */
    private $priorityName;
    /**
     * @var string
     */
    private $requestorUID;
    /**
     * @var string|null
     */
    private $description;

    /**
     * @var Carbon
     */
    private $createdDate;
    /**
     * @var string
     */
    private $requestorName;
    /**
     * @var string
     */
    private $requestorEmail;
    /**
     * @var AssetCollection
     */
    private $assets;

    /**
     * @param int $id
     * @param int $typeID
     * @param string $title
     * @param int $accountID
     * @param int $statusID
     * @param string $statusName
     * @param int $priorityID
     * @param string $priorityName
     * @param string $requestorUID
     * @param string|null $description
     * @param Carbon $createdDate
     * @param string $requestorName
     * @param string $requestorEmail
     * @param AssetCollection $assets
     */
    public function __construct(
        int     $id,
        int     $typeID,
        string  $title,
        int     $accountID,
        int     $statusID,
        string  $statusName,
        int     $priorityID,
        string  $priorityName,
        string $requestorUID,
        ?string $description,
        Carbon  $createdDate,
        string  $requestorName,
        string  $requestorEmail,
        AssetCollection $assets = null
    ) {
        $this->id = $id;
        $this->typeID = $typeID;
        $this->title = $title;
        $this->accountID = $accountID;
        $this->statusID = $statusID;
        $this->statusName = $statusName;
        $this->priorityID = $priorityID;
        $this->priorityName = $priorityName;
        $this->requestorUID = $requestorUID;
        $this->description = $description;
        $this->createdDate = $createdDate;
        $this->requestorName = $requestorName;
        $this->requestorEmail = $requestorEmail;
        $this->assets = $assets;
    }

    /**
     * @param array $data
     * @return TDTicket
     * @throws InvalidTicketDataException
     */
    public static function createFromArray(array $data): TDTicket
    {
        self::validateData($data);
        return new TDTicket(
            $data['ID'],
            $data['TypeID'],
            $data['Title'],
            $data['AccountID'],
            $data['StatusID'],
            $data['StatusName'],
            $data['PriorityID'],
            $data['PriorityName'],
            $data['RequestorUid'],
            $data['Description'],
            Carbon::parse($data['CreatedDate']),
            $data['RequestorName'],
            $data['RequestorEmail']
        );
    }

    /**
     * @param string $body
     * @return TDTicket
     * @throws InvalidTicketDataException
     */
    public static function createFromJson(string $body): TDTicket
    {
        $data = json_decode($body, true);
        return self::createFromArray($data);
    }

    /**
     * @param array $data
     * @throws InvalidTicketDataException
     */
    private static function validateData(array $data): void
    {
        if (!isset($data['ID'])) {
            throw new InvalidTicketDataException("Error: Ticket data has no ID");
        }

        if ($data['ID'] == -1) {
            throw new InvalidTicketDataException("Error: API encountered an error creating a ticket - " . $data['Message']);
        }

        if (!isset($data['Title'])) {
            throw new InvalidTicketDataException("Error: Ticket data has no Title");
        }

        if (!isset($data['TypeID'])) {
            throw new InvalidTicketDataException("Error: Ticket data has no TypeID");
        }

        if (!isset($data['AccountID'])) {
            throw new InvalidTicketDataException("Error: Ticket data has no AccountID");
        }

        if (!isset($data['RequestorUid'])) {
            throw new InvalidTicketDataException("Error: Ticket data has no RequestorUid");
        }
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return int
     */
    public function getTypeID(): int
    {
        return $this->typeID;
    }

    /**
     * @return string
     */
    public function getTitle(): string
    {
        return $this->title;
    }

    /**
     * @return int
     */
    public function getAccountID(): int
    {
        return $this->accountID;
    }

    /**
     * @return int
     */
    public function getStatusID(): int
    {
        return $this->statusID;
    }

    /**
     * @return int
     */
    public function getPriorityID(): int
    {
        return $this->priorityID;
    }

    /**
     * @return string
     */
    public function getRequestorUID(): string
    {
        return $this->requestorUID;
    }

    /**
     * @return string
     */
    public function getDescription(): string
    {
        return $this->description;
    }

    /**
     * @return string
     */
    public function getStatusName(): string
    {
        return $this->statusName;
    }

    /**
     * @return string
     */
    public function getPriorityName(): string
    {
        return $this->priorityName;
    }

    /**
     * @return Carbon
     */
    public function getCreatedDate(): Carbon
    {
        return $this->createdDate;
    }

    /**
     * @return string
     */
    public function getRequestorName(): string
    {
        return $this->requestorName;
    }

    /**
     * @return string
     */
    public function getRequestorEmail(): string
    {
        return $this->requestorEmail;
    }

    /**
     * @return AssetCollection
     */
    public function getAssets(): AssetCollection
    {
        return $this->assets;
    }

    public function addAssets(AssetCollection $assets): void
    {
        $this->assets = $assets;
    }

    /**
     * @return array
     */
    public function toArray(): array
    {
        return [
            'ID' => $this->getId(),
            'TypeID' => $this->getTypeID(),
            'Title' => $this->getTitle(),
            'AccountID' => $this->getAccountID(),
            'StatusID' => $this->getStatusID(),
            'StatusName' => $this->getStatusName(),
            'PriorityID' => $this->getPriorityID(),
            'PriorityName' => $this->getPriorityName(),
            'RequestorUid' => $this->getRequestorUID(),
            'Description' => $this->getDescription(),
            'CreatedDate' => $this->getCreatedDate(),
            'RequestorName' => $this->getRequestorName(),
            'RequestorEmail' => $this->getRequestorEmail()
        ];
    }
}
