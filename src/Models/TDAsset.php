<?php

namespace MiamiOH\TeamDynamix\Models;

use JetBrains\PhpStorm\ArrayShape;
use MiamiOH\TeamDynamix\Exceptions\InvalidAssetDataException;

class TDAsset extends BaseModel
{
    /**
     * @var string
     */
    private $name;
    /**
     * @var string
     */
    private $id;
    /**
     * @var string
     */
    private $productionURL;
    /**
     * @var string
     */
    private $gitRepoUrl;
    /**
     * @var string
     */
    private $testURL;
    /**
     * @var string
     */
    private $issueBacklogURL;
    /**
     * @var string
     */
    private $description;
    /**
     * @var string
     */
    private $diagramURL;

    /**
     * Asset constructor.
     * @param string $name
     * @param string $id
     * @param string $prodURL
     * @param string $testURL
     * @param string $gitURL
     * @param string $issueBacklogURL
     * @param string $description
     * @param string $diagramURL
     */
    public function __construct(
        string $name,
        string $id,
        string $prodURL = '',
        string $testURL = '',
        string $gitURL = '',
        string $issueBacklogURL = '',
        string $description = '',
        string $diagramURL = ''
    ) {
        $this->name = $name;
        $this->id = $id;
        $this->productionURL = $prodURL;
        $this->testURL = $testURL;
        $this->gitRepoUrl = $gitURL;
        $this->issueBacklogURL = $issueBacklogURL;
        $this->description = $description;
        $this->diagramURL = $diagramURL;
    }


    /**
     * @param array $data
     * @throws InvalidAssetDataException
     */
    private static function validateData(
        array $data
    ) {
        if (empty($data['Name'])) {
            throw new InvalidAssetDataException('Asset ' . $data['ID'] . ' missing a Name');
        }
        if (!isset($data['AssetID']) and !isset($data['ID'])) {
            throw new InvalidAssetDataException('Asset ' . $data['Name'] . ' missing an ID');
        }
    }

    /**
     * @param string $body
     * @return TDAsset
     * @throws InvalidAssetDataException
     */
    public static function createFromJson(
        string $body
    ): TDAsset {
        $data = \GuzzleHttp\json_decode($body, true);
        return self::createFromArray($data);
    }

    /**
     * @param array $data
     * @return TDAsset
     * @throws InvalidAssetDataException
     */
    public static function createFromArray(array $data): TDAsset
    {
        $prodUrl = '';
        $testUrl = '';
        $gitUrl = '';
        $issueBacklog = '';
        $description = '';
        $diagramUrl = '';
        self::validateData($data);
        if (isset($data['Attributes'])) {
            $attributes = collect($data['Attributes']);
            $prodUrl = self::extractFromAttributes($attributes, 'Prod URL');
            $gitUrl = self::extractFromAttributes($attributes, 'Git Repository');
            $testUrl = self::extractFromAttributes($attributes, 'Test URL');
            $issueBacklog = self::extractFromAttributes($attributes, 'Product Backlog');
            $description = self::extractFromAttributes($attributes, 'Description');
            $diagramUrl = self::extractFromAttributes($attributes, 'Diagram Link');
        }

        return new TDAsset($data['Name'], $data['AssetID'] ?? $data['ID'], $prodUrl, $testUrl, $gitUrl, $issueBacklog, $description, $diagramUrl);
    }

    /**
     * @return array
     */
    public function toArray(): array
    {
        return [
            'ID' => $this->getId(),
            'Name' => $this->getName(),
        ];
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @return string
     */
    public function productionURL(): string
    {
        return $this->productionURL;
    }

    /**
     * @return string
     */
    public function testURL(): string
    {
        return $this->testURL;
    }

    /**
     * @return string
     */
    public function issueBacklogURL(): string
    {
        return $this->issueBacklogURL;
    }

    /**
     * @return string
     */
    public function sourceControlURL(): string
    {
        return $this->gitRepoUrl;
    }

    /**
     * @return string
     */
    public function getId(): string
    {
        return $this->id;
    }

    public function getDescription(): string
    {
        return $this->description;
    }
    public function diagramLink(): string
    {
        return $this->diagramURL;
    }
}
