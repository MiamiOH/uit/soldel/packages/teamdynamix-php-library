# TeamDynamix PHP Library

## General Info
This is a PHP Library used to connect to the TeamDynamix APIs

# Installation

# Installation
## Add the Satis Repo
This package is managed via GitLab and published on our Satis server. If necessary, add the satis repo to your composer.json file:

```
    "repositories": [
        {
            "type": "composer",
            "url": "https://satis.itapps.miamioh.edu/miamioh"
        }
    ]
```
## Composer Install
To use the TeamDynamix Package, all that is needed is to first run the following command from terminal.
```
composer require "miamioh/teamdynamix-php-library"

# Usage

## Environment Variables
```
TDX_HOST=
TDX_USER=
TDX_PASS=
TDX_APPID=
TDX_OPEN_STATUS_CLASS_IDS=

(TDX_OPEN_STATUS_CLASS_IDS is not required, it is only used to overwrite values in Tickets.php)

## TDClient
TDClient is the class that will make your GET and POST requests, as well as getting an auth token to be used in the requests.

To construct a TDClient you need the following parameters:
```
appId
client
username
password
(optional) notifyRequestor
```

**appId**: The app ID of the TDX app that will be used

**client**: GuzzleHttp Client configured to your API host

**username**: Username of the TDX user that will be making the requests

**password**: Password for the TDX user defined above

**(optional) notifyRequestor**: This is used when creating new tickets via the API. It is used to let TDX know to notify the requestor (who created the ticket) that their ticket has been created. It defaults to true and does not change the request. If 'notifyRequestor' is set to false, it will append '?NotifyRequestor=false' to the request, and the requestor will not be notified that their ticket was created. The default behavior in TDX is to notify the requestor if '?NotifyRequestor' is missing.```

## Contributing to this package
See the [Contributing.md](CONTRIBUTING.md) file for information on how to contribute to this package.
