<?php

namespace Tests\MiamiOH\TeamDynamix\Unit\Models;

use MiamiOH\TeamDynamix\Exceptions\InvalidUserDataException;
use MiamiOH\TeamDynamix\Models\TDUser;
use Tests\MiamiOH\TeamDynamix\Unit\TDTestCase;

class TDUserTest extends TDTestCase
{
    private function testUser(): TDUser
    {
        return new TDUser(
            'ef7af242-a3d7-e311-bccd-0010188dda92',
            'Test',
            'Person',
            'notreal@miamioh.edu'
        );
    }

    public function testConvertTDUserToArray(): void
    {
        $this->assertSame(
            [
            'UID' => 'ef7af242-a3d7-e311-bccd-0010188dda92',
            'FirstName' => 'Test',
            'LastName' => 'Person',
            'PrimaryEmail' => 'notreal@miamioh.edu'
        ],
            $this->testUser()->toArray()
        );
    }

    public function testConvertTDUserToJson(): void
    {
        $this->assertSame(
            $this->sampleTDUserToJson(),
            $this->testUser()->toJson()
        );
    }

    public function testCreateTDUserFromJson(): void
    {
        $tdUser = TDUser::createFromJson($this->sampleUserResponse());
        $this->assertEquals($this->testUser(), $tdUser);
    }

    public function testCreateTDUserFromArray(): void
    {
        $tdUser = TDUser::createFromArray(json_decode($this->sampleUserResponse(), true));
        $this->assertEquals($this->testUser(), $tdUser);
    }

    public function testCreateInvalidTDUser(): void
    {
        $this->expectException(InvalidUserDataException::class);
        TDUser::createFromArray([]);
    }

}
