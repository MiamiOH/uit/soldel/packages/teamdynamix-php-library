<?php

namespace Tests\MiamiOH\TeamDynamix\Unit\Models;

use MiamiOH\TeamDynamix\Models\TDArticle;
use PHPUnit\Framework\TestCase;

class TDArticleTest extends TestCase
{
    public function testCanCreateFromArray(): void
    {
        $articleData = ['Subject' => 'Test Article','ID'=>111, 'AppID'=>123, 'CategoryName'=>'Test Category','IsPublished' => true];
        $testArticle = TDArticle::createFromArray($articleData);
        $this->assertEquals($articleData['Subject'], $testArticle->getTitle());
    }

    public function testCanGetAppId(): void
    {
        $articleData = ['Subject' => 'Test Article','ID'=>111, 'AppID'=>123, 'CategoryName'=>'Test Category','IsPublished' => true];
        $testArticle = TDArticle::createFromArray($articleData);
        $this->assertEquals($articleData['AppID'], $testArticle->getAppId());
    }
    public function testCanGetId(): void
    {
        $articleData = ['Subject' => 'Test Article','ID'=>111, 'AppID'=>123, 'CategoryName'=>'Test Category','IsPublished' => true];
        $testArticle = TDArticle::createFromArray($articleData);
        $this->assertEquals($articleData['ID'], $testArticle->getId());
    }

    public function testCanGetTitle(): void
    {
        $articleData = ['Subject' => 'Test Article','ID'=>111, 'AppID'=>123, 'CategoryName'=>'Test Category','IsPublished' => true];
        $testArticle = TDArticle::createFromArray($articleData);
        $this->assertEquals($articleData['Subject'], $testArticle->getTitle());
    }

    public function testCanReturnURL()
    {
        $articleData = ['Subject' => 'Test Article','ID'=>111, 'AppID'=>123, 'CategoryName'=>'Test Category','IsPublished' => true];
        $testArticle = TDArticle::createFromArray($articleData);
        $this->assertEquals('https://miamioh.teamdynamix.com/TDClient/123/Portal/KB/ArticleDet?ID=111', $testArticle->getArticleURL());
    }
    public function testCanReturnURLwithUpdatedBaseUrl()
    {
        $articleData = ['Subject' => 'Test Article','ID'=>111, 'AppID'=>123, 'CategoryName'=>'Test Category','IsPublished' => true,'baseUrl'=>'local/'];
        $testArticle = TDArticle::createFromArray($articleData);
        $this->assertEquals('local/123/Portal/KB/ArticleDet?ID=111', $testArticle->getArticleURL());
    }

}
