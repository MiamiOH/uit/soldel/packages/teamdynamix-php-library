<?php

namespace Tests\MiamiOH\TeamDynamix\Unit\Models;

use MiamiOH\TeamDynamix\Models\TDUser;
use Tests\MiamiOH\TeamDynamix\Unit\TDTestCase;

class BaseModelTest extends TDTestCase
{
    public function testToJson(): void
    {
        $user = new TDUser(
            'ef7af242-a3d7-e311-bccd-0010188dda92',
            'Test',
            'Person',
            'notreal@miamioh.edu'
        );

        $this->assertEquals($this->sampleTDUserToJson(), $user->toJson());
    }
}
