<?php

namespace Tests\MiamiOH\TeamDynamix\Unit\Models;

use MiamiOH\TeamDynamix\Models\TDAsset;
use Tests\MiamiOH\TeamDynamix\Unit\TDTestCase;

class TDAssetTest extends TDTestCase
{
    public function testCanCreateModelFromArray()
    {
        $startArray = $this->simpleAssetResponse();
        $testAsset = TDAsset::createFromArray($startArray);
        $this->assertEquals($startArray['Name'], $testAsset->getName());
        $this->assertEquals($startArray['ID'], $testAsset->getId());
    }

    public function testCanCreateModelFromJson()
    {
        $testAsset = TDAsset::createFromJson($this->fullSampleAssetResponse());
        $this->assertEquals('Configuration and Authorization Management (CAM)', $testAsset->getName());
    }

    public function testCanReturnDescription()
    {
        $testAsset = TDAsset::createFromJson($this->fullSampleAssetResponse());
        $descriptionString = "Configuration Authorization Management (CAM) is a streamlined, web-based user interface that combines the capabilities of Configuration Manager and Authorization Manager, and simplifies the configuration and authorization management of applications. CAM is designed to give users a single point of access to the AuthMan and ConfigMgr tools, where application administrators can be granted defined authorization to manage other authorizations and/or configurations for specific applications. CAM is written in PHP 7.2, a secure and supported version of the popular scripting language, and was developed for use by Solution Delivery developers, Application Operations analysts, and authorized client-office administrators, who manage authorization, configuration, and risk.";
        $this->assertEquals($descriptionString, $testAsset->getDescription());
    }
    public function testCanHandleBlankDescriptions()
    {
        $sampleDescription = ["Name"=> "Description","Value"=>""];
        $startArray = $this->simpleAssetResponse(["Attributes" => [$sampleDescription]]);
        $testAsset = TDAsset::createFromArray($startArray);
        $this->assertEquals('', $testAsset->getDescription());
    }

    public function testWillReturnEmptyStringWhenNoDescriptionFound()
    {
        $testAsset = TDAsset::createFromArray($this->simpleAssetResponse());
        $this->assertEquals('', $testAsset->getDescription());
    }
    public function testCanReturnProdUrl()
    {
        $testAsset = TDAsset::createFromJson($this->fullSampleAssetResponse());
        $this->assertEquals('https://www.apps.miamioh.edu/cam/', $testAsset->productionURL());
    }
    public function testCanReturnTestUrl()
    {
        $testAsset = TDAsset::createFromJson($this->fullSampleAssetResponse());
        $this->assertEquals('https://wwwtest.apps.miamioh.edu/cam/', $testAsset->testURL());
    }
    public function testCanReturnSourceControlUrl()
    {
        $testAsset = TDAsset::createFromJson($this->fullSampleAssetResponse());
        $this->assertEquals('https://gitlab.com/MiamiOH/uit/soldel/development/cam', $testAsset->sourceControlURL());
    }
    public function testCanReturnIssueBacklogURL()
    {
        $testAsset = TDAsset::createFromJson($this->fullSampleAssetResponse());
        $this->assertEquals('', $testAsset->diagramLink());

    }
    public function testCanReturnDiagramLink()
    {
        $sampleDescription = ["Name"=> "Diagram Link","Value"=>"https://someLinkToDiagrams"];
        $startArray = $this->simpleAssetResponse(["Attributes" => [$sampleDescription]]);
        $testAsset = TDAsset::createFromArray($startArray);
        $this->assertEquals('https://someLinkToDiagrams', $testAsset->diagramLink());
    }




}
