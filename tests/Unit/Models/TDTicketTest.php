<?php

namespace Tests\MiamiOH\TeamDynamix\Unit\Models;

use Carbon\Carbon;
use MiamiOH\TeamDynamix\Exceptions\InvalidTicketDataException;
use MiamiOH\TeamDynamix\Models\TDTicket;
use Tests\MiamiOH\TeamDynamix\Unit\TDTestCase;
use MiamiOH\TeamDynamix\Collections\AssetCollection;
class TDTicketTest extends TDTestCase
{
    public function testConvertTDTicketToArray(): void
    {
        $this->assertEquals(
            [
            'ID' => 13130054,
            'TypeID' => 6794,
            'Title' => 'Request: change status from former employee to former graduate student.',
            'AccountID' => 20050,
            'StatusID' => 1082,
            'StatusName' => 'Open',
            'PriorityID' => 1925,
            'PriorityName' => '5',
            'RequestorUid' => 'ef7af242-a3d7-e311-bccd-0010188dda92',
            'Description' => "Please assign this ticket to the 'application operation' group.  \r\n  \r\n  \r\n  \r\n \r\n  \r\n \r\n   Hi friends, \r\n  \r\n  \r\n  \r\n \r\n  \r\n \r\n   In order to keep my Miami email. Can someone change my status from a former employee to former graduate student?  \r\n  \r\n  \r\n  \r\n \r\n  \r\n \r\n   Thanks. :)  \r\n  \r\n  \r\n  \r\n \r\n   \r\n   \r\n \r\n  -- \r\n  \r\n \r\n   \r\n    \r\n     \r\n      \r\n       \r\n       \r\n         Weiguo Xia \r\n        \r\n       \r\n         Was an Application Developer@ \r\n        IT-Services",
            'CreatedDate' =>  Carbon::parse("2020-04-07T14:08:15.233Z"),
            'RequestorName' => 'Weiguo Xia',
            'RequestorEmail' => 'xiaw@miamioh.edu'
        ],
            $this->sampleTDTicket()->toArray()
        );
    }

    public function testConvertTDTicketToJson(): void
    {
        $this->assertSame(
            '{"ID":13130054,"TypeID":6794,"Title":"Request: change status from former employee to former graduate student.","AccountID":20050,"StatusID":1082,"StatusName":"Open","PriorityID":1925,"PriorityName":"5","RequestorUid":"ef7af242-a3d7-e311-bccd-0010188dda92","Description":"Please assign this ticket to the \'application operation\' group.  \r\n  \r\n  \r\n  \r\n \r\n  \r\n \r\n   Hi friends, \r\n  \r\n  \r\n  \r\n \r\n  \r\n \r\n   In order to keep my Miami email. Can someone change my status from a former employee to former graduate student?  \r\n  \r\n  \r\n  \r\n \r\n  \r\n \r\n   Thanks. :)  \r\n  \r\n  \r\n  \r\n \r\n   \r\n   \r\n \r\n  -- \r\n  \r\n \r\n   \r\n    \r\n     \r\n      \r\n       \r\n       \r\n         Weiguo Xia \r\n        \r\n       \r\n         Was an Application Developer@ \r\n        IT-Services","CreatedDate":"2020-04-07T14:08:15.233000Z","RequestorName":"Weiguo Xia","RequestorEmail":"xiaw@miamioh.edu"}',
            $this->sampleTDTicket()->toJson()
        );
    }

    public function testCreateFromJson(): void
    {
        $this->assertEquals($this->sampleTDTicket(), TDTicket::createFromJson($this->sampleIssueResponse()));
    }

    public function testCreateFromArray(): void
    {
        $actualTicket = TDTicket::createFromArray(json_decode($this->sampleIssueResponse(), true));
        $this->assertEquals($this->sampleTDTicket(), $actualTicket);
    }

    public function testCreateTDTicketWithoutID(): void
    {
        $this->expectException(InvalidTicketDataException::class);
        TDTicket::createFromArray([]);
    }

    public function testCreateInvalidTDTicket(): void
    {
        $this->expectException(InvalidTicketDataException::class);
        TDTicket::createFromArray(['ID' => -1, 'Message' => "search must not be null."]);
    }
    public function testCanGetAssets(): void
    {
        $actualTicket = TDTicket::createFromArray(json_decode($this->sampleIssueResponse(), true));
        $actualTicket->addAssets(new AssetCollection());
        $this->assertInstanceOf(AssetCollection::class, $actualTicket->getAssets());
    }
}
