<?php

namespace Tests\MiamiOH\TeamDynamix\Unit;

use Carbon\Carbon;
use GuzzleHttp\Client;
use GuzzleHttp\Handler\MockHandler;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\Middleware;
use GuzzleHttp\Psr7\Response;
use MiamiOH\TeamDynamix\Api\Assets;
use MiamiOH\TeamDynamix\Api\Tickets;
use MiamiOH\TeamDynamix\Models\TDTicket;
use MiamiOH\TeamDynamix\TDClient;
use PHPUnit\Framework\MockObject\MockObject;
use Tests\MiamiOH\TeamDynamix\TestCase;

class TDTestCase extends TestCase
{
    /**
     * @var MockObject
     */
    protected $mockTdClient;
    /**
     * @var Assets
     */
    protected $assetApi;

    /**
     * @var Tickets
     */
    protected $ticketsApi;

    public function setUp(): void
    {
        $this->mockTdClient = $this->getMockBuilder(TDClient::class)
            ->disableOriginalConstructor()
            ->onlyMethods([
                'get',
                'post',
                'getToken',
                'getAppId'
            ])->getMock();

        $this->mockTdClient
            ->method('getAppId')
            ->willReturn("123");
        $this->mockTdClient
            ->method('getToken')
            ->willReturn('123');

        $this->ticketsApi = new Tickets($this->mockTdClient);
        $this->assetApi = new Assets($this->mockTdClient);

    }

    public function fullSampleAssetResponse(): string
    {
        return '{"ID":528369,"AppID":741,"AppName":"IT CMDB","FormID":38150,"FormName":"Application / Software","ProductModelID":1070,"ProductModelName":"Application/Software","ManufacturerID":1030,"ManufacturerName":"Miami University","SupplierID":1030,"SupplierName":"Miami University","StatusID":641,"StatusName":"In Use","LocationID":0,"LocationName":"None","LocationRoomID":0,"LocationRoomName":"None","Tag":null,"SerialNumber":"CAM, SolDel Application Manager, appman, appmgr","Name":"Configuration and Authorization Management (CAM)","PurchaseCost":0.0,"AcquisitionDate":"0001-01-01T00:00:00","ExpectedReplacementDate":"0001-01-01T00:00:00","RequestingCustomerID":"aa7b2b51-32dc-e311-bccd-0010188dda92","RequestingCustomerName":"Dirk Tepe","RequestingDepartmentID":15798,"RequestingDepartmentName":"Information Technology Services","OwningCustomerID":"a70e5b6d-0767-eb11-8b7d-000d3a9b77a1","OwningCustomerName":"Anthony Dilisio","OwningDepartmentID":71254,"OwningDepartmentName":"Iterators","ParentID":0,"ParentSerialNumber":"None","ParentName":null,"ParentTag":null,"MaintenanceScheduleID":0,"MaintenanceScheduleName":"None","ConfigurationItemID":581379,"CreatedDate":"2019-08-19T13:41:15.083Z","CreatedUid":"7101e525-091b-e811-a95d-000d3a137856","CreatedFullName":"Akash Kamandula","ModifiedDate":"2024-10-25T18:43:33.59Z","ModifiedUid":"e698363b-7bcb-e511-b52c-000d3a113e84","ModifiedFullName":"Em Smith","ExternalID":"Configuration and Authorization Management (CAM)","ExternalSourceID":0,"ExternalSourceName":"None","Attributes":[{"ID":71473,"Name":"Application Language(s)","Order":0,"Description":"","SectionID":0,"SectionName":null,"FieldType":"multiselect","DataType":"String","Choices":[{"ID":176399,"Name":"Bash","IsActive":true,"DateCreated":"2019-02-28T19:54:16.167Z","DateModified":"2019-02-28T19:54:21.91Z","Order":0},{"ID":176400,"Name":"C","IsActive":true,"DateCreated":"2019-02-28T19:54:24.86Z","DateModified":"2019-02-28T19:54:24.86Z","Order":0},{"ID":176401,"Name":"C#","IsActive":true,"DateCreated":"2019-02-28T19:54:30.703Z","DateModified":"2019-02-28T19:54:30.703Z","Order":0},{"ID":176402,"Name":"C++","IsActive":true,"DateCreated":"2019-02-28T19:54:33.187Z","DateModified":"2019-02-28T19:54:33.187Z","Order":0},{"ID":176403,"Name":"COBOL","IsActive":true,"DateCreated":"2019-02-28T19:54:35.97Z","DateModified":"2019-02-28T19:54:35.97Z","Order":0},{"ID":176404,"Name":"ColdFusion","IsActive":true,"DateCreated":"2019-02-28T19:54:40.637Z","DateModified":"2019-02-28T19:54:40.637Z","Order":0},{"ID":176405,"Name":"HTML & CSS","IsActive":true,"DateCreated":"2019-02-28T19:54:45.733Z","DateModified":"2019-02-28T19:54:45.733Z","Order":0},{"ID":176406,"Name":"Java","IsActive":true,"DateCreated":"2019-02-28T19:54:48.583Z","DateModified":"2019-02-28T19:54:48.583Z","Order":0},{"ID":176407,"Name":"Javascript","IsActive":true,"DateCreated":"2019-02-28T19:54:53.277Z","DateModified":"2019-02-28T19:54:53.277Z","Order":0},{"ID":176408,"Name":"PERL","IsActive":true,"DateCreated":"2019-02-28T19:54:56.11Z","DateModified":"2019-02-28T19:54:56.11Z","Order":0},{"ID":176409,"Name":"PHP","IsActive":true,"DateCreated":"2019-02-28T19:54:59.087Z","DateModified":"2019-02-28T19:54:59.087Z","Order":0},{"ID":176410,"Name":"PL/SQL","IsActive":true,"DateCreated":"2019-02-28T19:55:03.68Z","DateModified":"2019-02-28T19:55:03.68Z","Order":0},{"ID":176411,"Name":"PowerShell","IsActive":true,"DateCreated":"2019-02-28T19:55:08.517Z","DateModified":"2019-02-28T19:55:08.517Z","Order":0},{"ID":176412,"Name":"Python","IsActive":true,"DateCreated":"2019-02-28T19:55:11.657Z","DateModified":"2019-02-28T19:55:11.657Z","Order":0},{"ID":176413,"Name":"Ruby","IsActive":true,"DateCreated":"2019-02-28T19:55:14.123Z","DateModified":"2019-02-28T19:55:14.123Z","Order":0},{"ID":176414,"Name":"SQR","IsActive":true,"DateCreated":"2019-02-28T19:55:16.88Z","DateModified":"2019-02-28T19:55:16.88Z","Order":0},{"ID":412826,"Name":"Terraform","IsActive":true,"DateCreated":"2021-09-15T15:42:54.27Z","DateModified":"2021-09-15T15:42:54.27Z","Order":0},{"ID":176415,"Name":"T-SQL","IsActive":true,"DateCreated":"2019-02-28T19:55:21.063Z","DateModified":"2019-02-28T19:55:21.063Z","Order":0},{"ID":176417,"Name":"XML","IsActive":true,"DateCreated":"2019-02-28T19:55:24.14Z","DateModified":"2019-02-28T19:55:24.14Z","Order":0},{"ID":176418,"Name":"Other","IsActive":true,"DateCreated":"2019-02-28T19:55:26.287Z","DateModified":"2019-02-28T19:55:53.72Z","Order":1},{"ID":176420,"Name":"No Selection","IsActive":true,"DateCreated":"2019-02-28T19:55:31.757Z","DateModified":"2019-02-28T19:55:49.293Z","Order":2}],"IsRequired":false,"IsUpdatable":false,"Value":"176409","ValueText":"PHP","ChoicesText":"PHP","AssociatedItemIDs":[0]},{"ID":148545,"Name":"Target State","Order":0,"Description":"What enterprise application will subsume functionality in long-term","SectionID":0,"SectionName":null,"FieldType":"dropdown","DataType":"String","Choices":[{"ID":480927,"Name":"Maintain","IsActive":true,"DateCreated":"2023-09-20T18:43:46.27Z","DateModified":"2023-09-20T18:43:46.27Z","Order":0},{"ID":480928,"Name":"Other/Unknown (describe in Asset Description)","IsActive":true,"DateCreated":"2023-09-20T18:43:46.277Z","DateModified":"2023-09-20T18:43:46.277Z","Order":1},{"ID":480929,"Name":"Replace with Slate CRM","IsActive":true,"DateCreated":"2023-09-20T18:43:46.283Z","DateModified":"2023-09-21T13:30:00.89Z","Order":2},{"ID":492337,"Name":"Redevelop for Slate CRM","IsActive":true,"DateCreated":"2024-03-20T17:19:00.687Z","DateModified":"2024-03-20T17:19:00.687Z","Order":3},{"ID":480930,"Name":"Replace with RapidIdentity","IsActive":true,"DateCreated":"2023-09-20T18:43:46.29Z","DateModified":"2023-09-21T13:29:40.097Z","Order":3},{"ID":495111,"Name":"Redevelop for Workday Platform","IsActive":true,"DateCreated":"2024-04-18T21:04:45.787Z","DateModified":"2024-04-18T21:04:45.787Z","Order":4},{"ID":480931,"Name":"Replace with Workday Platform","IsActive":true,"DateCreated":"2023-09-20T18:43:46.297Z","DateModified":"2023-09-20T18:43:46.297Z","Order":4},{"ID":495112,"Name":"Redevelop for Workday Student","IsActive":true,"DateCreated":"2024-04-18T21:05:00.783Z","DateModified":"2024-04-18T21:05:00.783Z","Order":5},{"ID":480932,"Name":"Replace with Workday Student","IsActive":true,"DateCreated":"2023-09-20T18:43:46.303Z","DateModified":"2023-09-20T18:43:46.303Z","Order":5}],"IsRequired":false,"IsUpdatable":false,"Value":"480927","ValueText":"Maintain","ChoicesText":"Maintain","AssociatedItemIDs":[0]},{"ID":71474,"Name":"Platform(s)/Framework(s)","Order":0,"Description":"","SectionID":0,"SectionName":null,"FieldType":"multiselect","DataType":"String","Choices":[{"ID":176445,"Name":".NET","IsActive":true,"DateCreated":"2019-02-28T19:58:28.413Z","DateModified":"2019-02-28T19:58:28.413Z","Order":0},{"ID":176447,"Name":"Angular JS","IsActive":true,"DateCreated":"2019-02-28T19:58:33.27Z","DateModified":"2019-02-28T19:58:33.27Z","Order":0},{"ID":176448,"Name":"Bootstrap","IsActive":true,"DateCreated":"2019-02-28T19:58:37.383Z","DateModified":"2019-02-28T19:58:37.383Z","Order":0},{"ID":484043,"Name":"Container","IsActive":true,"DateCreated":"2023-11-22T19:41:30.65Z","DateModified":"2023-11-22T19:41:30.65Z","Order":0},{"ID":176450,"Name":"Grails","IsActive":true,"DateCreated":"2019-02-28T19:58:41.07Z","DateModified":"2019-02-28T19:58:41.07Z","Order":0},{"ID":176451,"Name":"Hibernate","IsActive":true,"DateCreated":"2019-02-28T19:58:44.427Z","DateModified":"2019-02-28T19:58:54.503Z","Order":0},{"ID":176452,"Name":"JQuery","IsActive":true,"DateCreated":"2019-02-28T19:58:48.843Z","DateModified":"2019-02-28T19:58:48.843Z","Order":0},{"ID":176454,"Name":"Laravel","IsActive":true,"DateCreated":"2019-02-28T19:59:00.45Z","DateModified":"2019-02-28T19:59:00.45Z","Order":0},{"ID":176455,"Name":"Marmot","IsActive":true,"DateCreated":"2019-02-28T19:59:04.137Z","DateModified":"2019-02-28T19:59:04.137Z","Order":0},{"ID":176456,"Name":"Propel","IsActive":true,"DateCreated":"2019-02-28T19:59:07.373Z","DateModified":"2019-02-28T19:59:07.373Z","Order":0},{"ID":461784,"Name":"RESTng","IsActive":true,"DateCreated":"2023-03-01T15:52:00.067Z","DateModified":"2023-03-01T15:52:00.067Z","Order":0},{"ID":426347,"Name":"TDX iPaaS","IsActive":true,"DateCreated":"2022-02-25T17:17:59.567Z","DateModified":"2022-02-25T17:17:59.567Z","Order":0},{"ID":176457,"Name":"Vue.js","IsActive":true,"DateCreated":"2019-02-28T19:59:10.943Z","DateModified":"2019-02-28T19:59:10.943Z","Order":0},{"ID":480041,"Name":"Workday Integration Cloud","IsActive":true,"DateCreated":"2023-09-11T14:25:11.2Z","DateModified":"2023-09-11T14:25:11.2Z","Order":0},{"ID":176458,"Name":"No Selection","IsActive":true,"DateCreated":"2019-02-28T19:59:16.293Z","DateModified":"2019-02-28T19:59:24.21Z","Order":2}],"IsRequired":false,"IsUpdatable":false,"Value":"176454","ValueText":"Laravel","ChoicesText":"Laravel","AssociatedItemIDs":[0]},{"ID":11636,"Name":"Escalation Instructions","Order":0,"Description":"This optional field describes how and in which situations tickets should be routed somewhere other than the owner\'s primary group. If empty, the ticket will be routed to the PRIMARY Group of the owner.","SectionID":0,"SectionName":null,"FieldType":"textarea","DataType":"String","Choices":[],"IsRequired":false,"IsUpdatable":false,"Value":"Issues regarding the consumption of authorization or configuration data within a dependent application should be routed according the asset of the dependent application.\r\n\r\nIssues managing authorization or configuration with CAM should be routed to Application Operations.","ValueText":"Issues regarding the consumption of authorization or configuration data within a dependent application should be routed according the asset of the dependent application.\r\n\r\nIssues managing authorization or configuration with CAM should be routed to Application Operations.","ChoicesText":"","AssociatedItemIDs":[0]},{"ID":71475,"Name":"Git Repository","Order":0,"Description":"URL for Git Repo","SectionID":0,"SectionName":null,"FieldType":"textbox","DataType":"String","Choices":[],"IsRequired":false,"IsUpdatable":false,"Value":"https://gitlab.com/MiamiOH/uit/soldel/development/cam","ValueText":"https://gitlab.com/MiamiOH/uit/soldel/development/cam","ChoicesText":"","AssociatedItemIDs":[0]},{"ID":88283,"Name":"Product Backlog","Order":0,"Description":"","SectionID":0,"SectionName":null,"FieldType":"textbox","DataType":"String","Choices":[],"IsRequired":false,"IsUpdatable":true,"Value":"https://miamioh.atlassian.net/jira/software/c/projects/CAM/issues/","ValueText":"https://miamioh.atlassian.net/jira/software/c/projects/CAM/issues/","ChoicesText":"","AssociatedItemIDs":[0]},{"ID":71476,"Name":"Prod URL","Order":0,"Description":"Full URL (no alias)","SectionID":0,"SectionName":null,"FieldType":"textbox","DataType":"String","Choices":[],"IsRequired":false,"IsUpdatable":false,"Value":"https://www.apps.miamioh.edu/cam/","ValueText":"https://www.apps.miamioh.edu/cam/","ChoicesText":"","AssociatedItemIDs":[0]},{"ID":71477,"Name":"Test URL","Order":0,"Description":"","SectionID":0,"SectionName":null,"FieldType":"textbox","DataType":"String","Choices":[],"IsRequired":false,"IsUpdatable":false,"Value":"https://wwwtest.apps.miamioh.edu/cam/","ValueText":"https://wwwtest.apps.miamioh.edu/cam/","ChoicesText":"","AssociatedItemIDs":[0]},{"ID":11632,"Name":"Description","Order":50,"Description":"","SectionID":0,"SectionName":null,"FieldType":"textarea","DataType":"String","Choices":[],"IsRequired":false,"IsUpdatable":false,"Value":"Configuration Authorization Management (CAM) is a streamlined, web-based user interface that combines the capabilities of Configuration Manager and Authorization Manager, and simplifies the configuration and authorization management of applications. CAM is designed to give users a single point of access to the AuthMan and ConfigMgr tools, where application administrators can be granted defined authorization to manage other authorizations and/or configurations for specific applications. CAM is written in PHP 7.2, a secure and supported version of the popular scripting language, and was developed for use by Solution Delivery developers, Application Operations analysts, and authorized client-office administrators, who manage authorization, configuration, and risk.","ValueText":"Configuration Authorization Management (CAM) is a streamlined, web-based user interface that combines the capabilities of Configuration Manager and Authorization Manager, and simplifies the configuration and authorization management of applications. CAM is designed to give users a single point of access to the AuthMan and ConfigMgr tools, where application administrators can be granted defined authorization to manage other authorizations and/or configurations for specific applications. CAM is written in PHP 7.2, a secure and supported version of the popular scripting language, and was developed for use by Solution Delivery developers, Application Operations analysts, and authorized client-office administrators, who manage authorization, configuration, and risk.","ChoicesText":"","AssociatedItemIDs":[0]},{"ID":121328,"Name":"Direct Confidence Score","Order":0,"Description":"On scale of one to 10: direct confidence in a service is determined by operational characteristics from development through runtime\nSee https://docs.google.com/document/d/1N9_f9AuoPqfgksK7Ijg0nxqSLP6HvONsDBrDlnB2d6E/edit?usp=sharing for signals","SectionID":0,"SectionName":null,"FieldType":"textbox","DataType":"Int32","Choices":[],"IsRequired":false,"IsUpdatable":false,"Value":"7","ValueText":"7","ChoicesText":"","AssociatedItemIDs":[0]},{"ID":121327,"Name":"Context Score","Order":0,"Description":"On a scale of 1 to 10:  describes the value and availability needs of the service from the user perspective.\nSee https://docs.google.com/document/d/1N9_f9AuoPqfgksK7Ijg0nxqSLP6HvONsDBrDlnB2d6E/edit?usp=sharing for signals to consider","SectionID":0,"SectionName":null,"FieldType":"textbox","DataType":"Int32","Choices":[],"IsRequired":false,"IsUpdatable":false,"Value":"6","ValueText":"6","ChoicesText":"","AssociatedItemIDs":[0]},{"ID":121329,"Name":"Complexity Score","Order":0,"Description":"On a scale of 1 to 10:  represents how hard a service is to understand as well it\'s exposure to external factors\nSee https://docs.google.com/document/d/1N9_f9AuoPqfgksK7Ijg0nxqSLP6HvONsDBrDlnB2d6E/edit?usp=sharing for signals to consider","SectionID":0,"SectionName":null,"FieldType":"textbox","DataType":"Int32","Choices":[],"IsRequired":false,"IsUpdatable":false,"Value":"3","ValueText":"3","ChoicesText":"","AssociatedItemIDs":[0]}],"Attachments":[{"ID":"d1954c55-6e2c-48df-8258-f6d463185e83","AttachmentType":27,"ItemID":528369,"CreatedUid":"e698363b-7bcb-e511-b52c-000d3a113e84","CreatedFullName":"Em Smith","CreatedDate":"2021-10-15T13:59:15.15Z","Name":"CAM Knowledge Map.xlsx","Size":0,"Uri":"api/attachments/d1954c55-6e2c-48df-8258-f6d463185e83","ContentUri":"https://drive.google.com/file/d/1Mo-4WYMvNpXSUT92jXKCxcASIxT-_Fvx/view?usp=drive_web"}],"Uri":"api/741/assets/528369"}';
    }

    public function sampleIssueResponse(): string
    {
        return '{"ID":13130054,"ParentID":0,"ParentTitle":"","ParentClass":0,"TypeID":6794,"TypeName":"Standard Service Request","TypeCategoryID":1147,"TypeCategoryName":"Service Request","Classification":32,"ClassificationName":"Incident","FormID":22026,"FormName":"KCS","Title":"Request: change status from former employee to former graduate student.","Description":"Please assign this ticket to the \'application operation\' group.  \r\n  \r\n  \r\n  \r\n \r\n  \r\n \r\n   Hi friends, \r\n  \r\n  \r\n  \r\n \r\n  \r\n \r\n   In order to keep my Miami email. Can someone change my status from a former employee to former graduate student?  \r\n  \r\n  \r\n  \r\n \r\n  \r\n \r\n   Thanks. :)  \r\n  \r\n  \r\n  \r\n \r\n   \r\n   \r\n \r\n  -- \r\n  \r\n \r\n   \r\n    \r\n     \r\n      \r\n       \r\n       \r\n         Weiguo Xia \r\n        \r\n       \r\n         Was an Application Developer@ \r\n        IT-Services","Uri":"api/50/tickets/13130054","AccountID":20050,"AccountName":"Other","SourceID":531,"SourceName":"Email","StatusID":1082,"StatusName":"Open","StatusClass":2,"ImpactID":1832,"ImpactName":"Small Impact","UrgencyID":1836,"UrgencyName":"Low","PriorityID":1925,"PriorityName":"5","PriorityOrder":5.0,"SlaID":128,"SlaName":"Priority 5 SLA","IsSlaViolated":false,"IsSlaRespondByViolated":false,"IsSlaResolveByViolated":false,"RespondByDate":"2020-04-07T15:08:15.233Z","ResolveByDate":"2020-05-04T13:39:00Z","SlaBeginDate":"2020-04-07T14:08:15.233Z","IsOnHold":false,"PlacedOnHoldDate":"0001-01-01T05:00:00Z","GoesOffHoldDate":"0001-01-01T05:00:00Z","CreatedDate":"2020-04-07T14:08:15.233Z","CreatedUid":"df8b47f1-bfae-e311-bccd-0010188dda92","CreatedFullName":"IT Help","CreatedEmail":"bbhelpdesk@miamioh.edu","ModifiedDate":"2020-05-01T04:10:24.02Z","ModifiedUid":"00000000-0000-0000-0000-000000000000","ModifiedFullName":" ","RequestorName":"Weiguo Xia","RequestorFirstName":"Weiguo","RequestorLastName":"Xia","RequestorEmail":"xiaw@miamioh.edu","RequestorPhone":"n/a","RequestorUid":"ef7af242-a3d7-e311-bccd-0010188dda92","ActualMinutes":0,"EstimatedMinutes":0,"DaysOld":24,"StartDate":null,"EndDate":null,"ResponsibleUid":"908c1f67-32dc-e311-bccd-0010188dda92","ResponsibleFullName":"Tim Ward","ResponsibleEmail":"wardtd@miamioh.edu","ResponsibleGroupID":3376,"ResponsibleGroupName":"Application Operations","RespondedDate":"2020-05-01T04:10:24.02Z","RespondedUid":null,"RespondedFullName":" ","CompletedDate":"0001-01-01T05:00:00Z","CompletedUid":null,"CompletedFullName":"","ReviewerUid":null,"ReviewerFullName":"","ReviewerEmail":"","ReviewingGroupID":0,"ReviewingGroupName":"","TimeBudget":0.0,"ExpensesBudget":0.0,"TimeBudgetUsed":0.0,"ExpensesBudgetUsed":0.0,"IsConvertedToTask":false,"ConvertedToTaskDate":"0001-01-01T05:00:00Z","ConvertedToTaskUid":null,"ConvertedToTaskFullName":"","TaskProjectID":0,"TaskProjectName":"","TaskPlanID":0,"TaskPlanName":"","TaskID":0,"TaskTitle":"","TaskStartDate":"0001-01-01T05:00:00Z","TaskEndDate":"0001-01-01T05:00:00Z","TaskPercentComplete":0,"LocationID":0,"LocationName":"","LocationRoomID":0,"LocationRoomName":"","RefCode":"2144-77002","ServiceID":9085,"ServiceName":"Identity and Access Management Overview","ServiceCategoryID":1456,"ServiceCategoryName":"Accounts, IDs & Passwords","ArticleID":21461,"ArticleSubject":"Expiration of my Miami accounts","ArticleStatus":3,"ArticleCategoryPathNames":"|Miami Email|Lifetime Email|","ArticleAppID":1813,"ArticleShortcutID":null,"AppID":50,"Attributes":[{"ID":9326,"Name":"Preferred Phone Number","Order":0,"Description":"","SectionID":0,"SectionName":null,"FieldType":"textbox","DataType":"String","Choices":[],"IsRequired":false,"IsUpdatable":true,"Value":"1111111111","ValueText":"1111111111","ChoicesText":"","AssociatedItemIDs":[0]},{"ID":9112,"Name":"Resolution Confirmation Preference","Order":0,"Description":"This field reflects the client\'s preference for receiving an update about the ticket resolution.\n\nIt will default to a phone call and an email for emailed tickets.","SectionID":0,"SectionName":null,"FieldType":"dropdown","DataType":"String","Choices":[{"ID":21535,"Name":"Call me, email me, then just close the ticket","IsActive":true,"DateCreated":"2014-05-22T15:04:43.327Z","DateModified":"2014-05-22T15:04:43.327Z","Order":0},{"ID":21537,"Name":"Do not close the ticket until you get in touch with me","IsActive":true,"DateCreated":"2014-05-22T15:05:25.13Z","DateModified":"2014-05-22T15:05:25.13Z","Order":0},{"ID":21536,"Name":"Send me an FYI email and close the ticket","IsActive":true,"DateCreated":"2014-05-22T15:05:06.46Z","DateModified":"2014-05-22T15:05:06.46Z","Order":0}],"IsRequired":false,"IsUpdatable":true,"Value":"21536","ValueText":"Send me an FYI email and close the ticket","ChoicesText":"Send me an FYI email and close the ticket","AssociatedItemIDs":[0]}],"Attachments":[{"ID":"43c4afa2-08ff-4b14-a42c-2af9583ee444","AttachmentType":9,"ItemID":13130054,"CreatedUid":"df8b47f1-bfae-e311-bccd-0010188dda92","CreatedFullName":"IT Help","CreatedDate":"2020-04-07T14:08:16.223Z","Name":"mail_content.html","Size":860,"Uri":"api/attachments/43c4afa2-08ff-4b14-a42c-2af9583ee444","ContentUri":"api/attachments/43c4afa2-08ff-4b14-a42c-2af9583ee444/content"},{"ID":"0d62c195-5716-4fda-aa92-eb314a8d2c59","AttachmentType":9,"ItemID":13130054,"CreatedUid":"df8b47f1-bfae-e311-bccd-0010188dda92","CreatedFullName":"IT Help","CreatedDate":"2020-04-07T14:08:16.46Z","Name":"mail_content.txt","Size":274,"Uri":"api/attachments/0d62c195-5716-4fda-aa92-eb314a8d2c59","ContentUri":"api/attachments/0d62c195-5716-4fda-aa92-eb314a8d2c59/content"}],"Tasks":[],"Notify":[{"ItemRole":"Responsible","Name":"Tim Ward","Initials":null,"Value":"wardtd@miamioh.edu","RefValue":0,"ProfileImageFileName":null},{"ItemRole":"Responsible Group","Name":"Application Operations","Initials":null,"Value":"3376","RefValue":0,"ProfileImageFileName":null},{"ItemRole":"Requestor","Name":"Weiguo Xia","Initials":null,"Value":"xiaw@miamioh.edu","RefValue":0,"ProfileImageFileName":null},{"ItemRole":"Creator","Name":"IT Help","Initials":null,"Value":"bbhelpdesk@miamioh.edu","RefValue":0,"ProfileImageFileName":null},{"ItemRole":"Contact","Name":"Eboney Kimbrough","Initials":null,"Value":"kimbroe@miamioh.edu","RefValue":0,"ProfileImageFileName":null}]}';
    }

    public function sampleAssetSearchResponse(): string
    {
        return '{"Data":[{"ID":10185,"AppID":741,"AppName":"Assets/CIs","FormID":38150,"FormName":"Application / Software","Title": "Request: change status from former employee to former graduate student.","ProductModelID":1070,"ProductModelName":"Application/Software","ManufacturerID":1030,"ManufacturerName":"Miami University","SupplierID":1031,"SupplierName":"Supplier","StatusID":641,"StatusName":"In Use","LocationID":0,"LocationName":"None","LocationRoomID":0,"LocationRoomName":"None","Tag":null,"SerialNumber":"Canvas","Name":"Canvas","PurchaseCost":0.0,"AcquisitionDate":"0001-01-01T05:00:00Z","ExpectedReplacementDate":"0001-01-01T05:00:00Z","RequestingCustomerID":"d748faa1-22e4-e811-a961-000d3a137856","RequestingCustomerName":"Ryan Baltrip","RequestingDepartmentID":16251,"RequestingDepartmentName":"Office of eLearning","OwningCustomerID":"ad74ac9b-a0b0-e311-bccd-0010188dda92","OwningCustomerName":"Daniel Johnson","OwningDepartmentID":60492,"OwningDepartmentName":"Agile Ninjas","ParentID":0,"ParentSerialNumber":"None","ParentName":null,"ParentTag":null,"MaintenanceScheduleID":0,"MaintenanceScheduleName":"None","ConfigurationItemID":16332,"CreatedDate":"2014-11-20T21:22:53.173Z","CreatedUid":"32c0ae47-8cdb-e311-bccd-0010188dda92","CreatedFullName":"Tim Gruenhagen","ModifiedDate":"2019-10-25T21:45:06.743Z","ModifiedUid":"df8b47f1-bfae-e311-bccd-0010188dda92","ModifiedFullName":"IT Help","ExternalID":"Canvas","ExternalSourceID":0,"ExternalSourceName":"None","Attributes":[],"Attachments":[],"Uri":"api/741/assets/10185"},{"ID":66438,"AppID":741,"AppName":"Assets/CIs","FormID":30878,"FormName":"Default Asset Form","ProductModelID":1070,"ProductModelName":"Application/Software","ManufacturerID":1030,"ManufacturerName":"Miami University","SupplierID":1031,"SupplierName":"Supplier","StatusID":641,"StatusName":"In Use","LocationID":0,"LocationName":"None","LocationRoomID":0,"LocationRoomName":"None","Tag":null,"SerialNumber":"","Name":"GitLab","PurchaseCost":0.0,"AcquisitionDate":"2016-02-08T05:00:00Z","ExpectedReplacementDate":"0001-01-01T05:00:00Z","RequestingCustomerID":"00000000-0000-0000-0000-000000000000","RequestingCustomerName":"None","RequestingDepartmentID":0,"RequestingDepartmentName":"None","OwningCustomerID":"302d16df-a0b0-e311-bccd-0010188dda92","OwningCustomerName":"Jeff Triplett","OwningDepartmentID":60516,"OwningDepartmentName":"Linux Administration","ParentID":0,"ParentSerialNumber":"None","ParentName":null,"ParentTag":null,"MaintenanceScheduleID":0,"MaintenanceScheduleName":"None","ConfigurationItemID":78372,"CreatedDate":"2016-02-08T19:09:02.063Z","CreatedUid":"6ffbf9b1-2edc-e311-bccd-0010188dda92","CreatedFullName":"David Schaefer","ModifiedDate":"2019-10-25T20:49:24.967Z","ModifiedUid":"df8b47f1-bfae-e311-bccd-0010188dda92","ModifiedFullName":"IT Help","ExternalID":"GitLab","ExternalSourceID":0,"ExternalSourceName":"None","Attributes":[],"Attachments":[],"Uri":"api/741/assets/66438"},{"ID":10700,"AppID":741,"AppName":"Assets/CIs","FormID":38150,"FormName":"Application / Software","ProductModelID":1070,"ProductModelName":"Application/Software","ManufacturerID":1030,"ManufacturerName":"Miami University","SupplierID":1031,"SupplierName":"Supplier","StatusID":641,"StatusName":"In Use","LocationID":0,"LocationName":"None","LocationRoomID":0,"LocationRoomName":"None","Tag":null,"SerialNumber":"NetDisk","Name":"NetDisk","PurchaseCost":0.0,"AcquisitionDate":"0001-01-01T05:00:00Z","ExpectedReplacementDate":"0001-01-01T05:00:00Z","RequestingCustomerID":"00000000-0000-0000-0000-000000000000","RequestingCustomerName":"None","RequestingDepartmentID":0,"RequestingDepartmentName":"None","OwningCustomerID":"6ffbf9b1-2edc-e311-bccd-0010188dda92","OwningCustomerName":"David Schaefer","OwningDepartmentID":71254,"OwningDepartmentName":"Iterators","ParentID":0,"ParentSerialNumber":"None","ParentName":null,"ParentTag":null,"MaintenanceScheduleID":0,"MaintenanceScheduleName":"None","ConfigurationItemID":16847,"CreatedDate":"2014-11-20T21:53:01.55Z","CreatedUid":"32c0ae47-8cdb-e311-bccd-0010188dda92","CreatedFullName":"Tim Gruenhagen","ModifiedDate":"2019-10-25T20:39:02.27Z","ModifiedUid":"df8b47f1-bfae-e311-bccd-0010188dda92","ModifiedFullName":"IT Help","ExternalID":"NetDisk","ExternalSourceID":0,"ExternalSourceName":"None","Attributes":[],"Attachments":[],"Uri":"api/741/assets/10700"},{"ID":431924,"AppID":741,"AppName":"Assets/CIs","FormID":38150,"FormName":"Application / Software","ProductModelID":1070,"ProductModelName":"Application/Software","ManufacturerID":1030,"ManufacturerName":"Miami University","SupplierID":1030,"SupplierName":"Miami University","StatusID":641,"StatusName":"In Use","LocationID":0,"LocationName":"None","LocationRoomID":0,"LocationRoomName":"None","Tag":null,"SerialNumber":"","Name":"RESTng Education Service","PurchaseCost":0.0,"AcquisitionDate":"0001-01-01T05:00:00Z","ExpectedReplacementDate":"0001-01-01T05:00:00Z","RequestingCustomerID":"0b87ca3a-2ce0-e311-bccd-0010188dda92","RequestingCustomerName":"Lindsay Carpenter","RequestingDepartmentID":75774,"RequestingDepartmentName":"Academic Affairs","OwningCustomerID":"ad74ac9b-a0b0-e311-bccd-0010188dda92","OwningCustomerName":"Daniel Johnson","OwningDepartmentID":71254,"OwningDepartmentName":"Iterators","ParentID":0,"ParentSerialNumber":"None","ParentName":null,"ParentTag":null,"MaintenanceScheduleID":0,"MaintenanceScheduleName":"None","ConfigurationItemID":481268,"CreatedDate":"2019-04-23T00:58:02.727Z","CreatedUid":"aa7b2b51-32dc-e311-bccd-0010188dda92","CreatedFullName":"Dirk Tepe","ModifiedDate":"2019-10-25T19:45:40.35Z","ModifiedUid":"c2dc0931-a3d7-e311-bccd-0010188dda92","ModifiedFullName":"Emily Schmidt","ExternalID":"","ExternalSourceID":0,"ExternalSourceName":"None","Attributes":[],"Attachments":[],"Uri":"api/741/assets/431924"},{"ID":10381,"AppID":741,"AppName":"Assets/CIs","FormID":38150,"FormName":"Application / Software","ProductModelID":1070,"ProductModelName":"Application/Software","ManufacturerID":1030,"ManufacturerName":"Miami University","SupplierID":1031,"SupplierName":"Supplier","StatusID":641,"StatusName":"In Use","LocationID":0,"LocationName":"None","LocationRoomID":0,"LocationRoomName":"None","Tag":null,"SerialNumber":"Tuition Management System (TMS)","Name":"Tuition Management System (TMS)","PurchaseCost":0.0,"AcquisitionDate":"0001-01-01T05:00:00Z","ExpectedReplacementDate":"0001-01-01T05:00:00Z","RequestingCustomerID":"1ac8da28-2ce0-e311-bccd-0010188dda92","RequestingCustomerName":"Kerri Jackson","RequestingDepartmentID":16284,"RequestingDepartmentName":"Bursar","OwningCustomerID":"dd8f14b1-31dc-e311-bccd-0010188dda92","OwningCustomerName":"William Miley","OwningDepartmentID":60552,"OwningDepartmentName":"East","ParentID":0,"ParentSerialNumber":"None","ParentName":null,"ParentTag":null,"MaintenanceScheduleID":0,"MaintenanceScheduleName":"None","ConfigurationItemID":16528,"CreatedDate":"2014-11-20T21:23:02.557Z","CreatedUid":"32c0ae47-8cdb-e311-bccd-0010188dda92","CreatedFullName":"Tim Gruenhagen","ModifiedDate":"2019-10-25T17:41:07.303Z","ModifiedUid":"65a411f5-b6e1-e311-bccd-0010188dda92","ModifiedFullName":"Rose Duh","ExternalID":"","ExternalSourceID":0,"ExternalSourceName":"None","Attributes":[],"Attachments":[],"Uri":"api/741/assets/10381"}],"TotalCount":4,"CurrentPageIndex":0,"PageSize":5}';
    }

    public function sampleUserResponse(): string
    {
        return '{"UID":"ef7af242-a3d7-e311-bccd-0010188dda92","ReferenceID":12347621,"BEID":"6d521a0a-6db6-4391-89ff-1a91ed6143f2","BEIDInt":21,"IsActive":true,"IsConfidential":false,"UserName":"","FullName":"Test Person","FirstName":"Test","LastName":"Person","MiddleName":null,"Salutation":null,"Nickname":null,"DefaultAccountID":15798,"DefaultAccountName":"Information Technology Services","PrimaryEmail":"notreal@miamioh.edu","AlternateEmail":null,"ExternalID":null,"AlternateID":null,"Applications":null,"SecurityRoleName":"","SecurityRoleID":"","Permissions":null,"OrgApplications":null,"PrimaryClientPortalApplicationID":null,"GroupIDs":null,"AlertEmail":"lakerc@miamioh.edu","ProfileImageFileName":"","Company":null,"Title":null,"HomePhone":null,"PrimaryPhone":"+1 800 867 5309","WorkPhone":null,"Pager":null,"OtherPhone":null,"MobilePhone":null,"Fax":null,"DefaultPriorityID":0,"DefaultPriorityName":"None","AboutMe":null,"WorkAddress":"","WorkCity":"","WorkState":"","WorkZip":null,"WorkCountry":"","HomeAddress":"","HomeCity":"","HomeState":"","HomeZip":"","HomeCountry":"","LocationID":0,"LocationName":null,"LocationRoomID":0,"LocationRoomName":null,"DefaultRate":0,"CostRate":0,"IsEmployee":false,"WorkableHours":8,"IsCapacityManaged":false,"ReportTimeAfterDate":"0001-01-01T00:00:00","EndDate":"0001-01-01T00:00:00","ShouldReportTime":false,"ReportsToUID":"","ReportsToFullName":"","ResourcePoolID":-1,"ResourcePoolName":"","TZID":0,"TZName":"","TypeID":0,"AuthenticationUserName":"","AuthenticationProviderID":null,"Attributes":null,"IMProvider":null,"IMHandle":null}';
    }

    public function sampleTDTicketToJson(): string
    {
        return '{"ID":13130054,"Title":"Request: change status from former employee to former graduate student.","Description":"Please assign this ticket to the \'application operation\' group.  \r\n  \r\n  \r\n  \r\n \r\n  \r\n \r\n   Hi friends, \r\n  \r\n  \r\n  \r\n \r\n  \r\n \r\n   In order to keep my Miami email. Can someone change my status from a former employee to former graduate student?  \r\n  \r\n  \r\n  \r\n \r\n  \r\n \r\n   Thanks. :)  \r\n  \r\n  \r\n  \r\n \r\n   \r\n   \r\n \r\n  -- \r\n  \r\n \r\n   \r\n    \r\n     \r\n      \r\n       \r\n       \r\n         Weiguo Xia \r\n        \r\n       \r\n         Was an Application Developer@ \r\n        IT-Services","RequestorUid":"ef7af242-a3d7-e311-bccd-0010188dda92","StatusID":1082,"AccountID":20050,"PriorityID":1925,"TypeID":6794}';
    }

    public function sampleTDUserToJson(): string
    {
        return '{"UID":"ef7af242-a3d7-e311-bccd-0010188dda92","FirstName":"Test","LastName":"Person","PrimaryEmail":"notreal@miamioh.edu"}';
    }
    public function sampleTicketWithMultipleAssets(): string
    {
        return '[{"ID":823113,"AppID":741,"AppName":"IT CMDB","FormID":30878,"FormName":"Default Asset Form","IsSystemMaintained":true,"BackingItemID":752169,"BackingItemType":27,"Name":"MUVPN - BlackBoard Support Staff","TypeID":1,"TypeName":"Asset","MaintenanceScheduleID":39,"MaintenanceScheduleName":"Sun Morning 2 am - 6 am. Weekdays 5 am - 7 am","OwnerUID":"6c15b476-a0b0-e311-bccd-0010188dda92","OwnerFullName":"Bob Black","OwningDepartmentID":60514,"OwningDepartmentName":"IT Process and Planning","OwningGroupID":0,"OwningGroupName":"","LocationID":0,"LocationName":"","LocationRoomID":0,"LocationRoomName":"","IsActive":true,"CreatedDateUtc":"2020-07-01T18:11:05.5766667","CreatedUid":"de7d6ce8-c0e1-e311-bccd-0010188dda92","CreatedFullName":"Josh Senn","ModifiedDateUtc":"2020-07-01T18:14:46.4133333","ModifiedUid":"de7d6ce8-c0e1-e311-bccd-0010188dda92","ModifiedFullName":"Josh Senn","ExternalID":null,"ExternalSourceID":0,"ExternalSourceName":"None","Attributes":[],"Attachments":[],"Uri":"api/741/cmdb/823113"},{"ID":16482,"AppID":741,"AppName":"IT CMDB","FormID":38150,"FormName":"Application / Software","IsSystemMaintained":true,"BackingItemID":10335,"BackingItemType":27,"Name":"Qualtrics","TypeID":1,"TypeName":"Asset","MaintenanceScheduleID":0,"MaintenanceScheduleName":"","OwnerUID":"2751f379-31dc-e311-bccd-0010188dda92","OwnerFullName":"Leah Harris","OwningDepartmentID":15798,"OwningDepartmentName":"Information Technology Services","OwningGroupID":0,"OwningGroupName":"","LocationID":0,"LocationName":"","LocationRoomID":0,"LocationRoomName":"","IsActive":true,"CreatedDateUtc":"2014-11-20T21:23:00.27","CreatedUid":"32c0ae47-8cdb-e311-bccd-0010188dda92","CreatedFullName":"Tim Gruenhagen","ModifiedDateUtc":"2019-03-19T17:25:27.6633333","ModifiedUid":"2751f379-31dc-e311-bccd-0010188dda92","ModifiedFullName":"Leah Harris","ExternalID":"Qualtrics","ExternalSourceID":0,"ExternalSourceName":"None","Attributes":[],"Attachments":[],"Uri":"api/741/cmdb/16482"}]';
    }
    public function sampleTicketWithSingleAsset(): string
    {
        return '[{
            "ID": 16429,
            "AppID": 741,
            "AppName": "IT CMDB",
            "FormID": 38150,
            "FormName": "Application / Software",
            "IsSystemMaintained": true,
            "BackingItemID": 10282,
            "BackingItemType": 27,
            "Name": "Lifetime Email for Alumni Sign-up Form",
            "TypeID": 1,
            "TypeName": "Asset",
            "MaintenanceScheduleID": 36,
            "MaintenanceScheduleName": "24x7 365",
            "OwnerUID": "9ff52cd7-31dc-e311-bccd-0010188dda92",
            "OwnerFullName": "Erin Mills",
            "OwningDepartmentID": 60530,
            "OwningDepartmentName": "The Sharks",
            "OwningGroupID": 0,
            "OwningGroupName": "",
            "LocationID": 0,
            "LocationName": "",
            "LocationRoomID": 0,
            "LocationRoomName": "",
            "IsActive": true,
            "CreatedDateUtc": "2014-11-20T21:22:58.097",
            "CreatedUid": "32c0ae47-8cdb-e311-bccd-0010188dda92",
            "CreatedFullName": "Tim Gruenhagen",
            "ModifiedDateUtc": "2022-06-16T13:10:31.92",
            "ModifiedUid": "c2dc0931-a3d7-e311-bccd-0010188dda92",
            "ModifiedFullName": "Emily Schmidt",
            "ExternalID": "Lifetime Email for Alumni Sign-up Form",
            "ExternalSourceID": 0,
            "ExternalSourceName": "None",
            "Attributes": [],
            "Attachments": [],
            "Uri": "api/741/cmdb/16429"
        }]';
    }

    public function sampleTDTicket(): TDTicket
    {
        return new TDTicket(
            13130054,
            6794,
            'Request: change status from former employee to former graduate student.',
            20050,
            1082,
            'Open',
            1925,
            '5',
            'ef7af242-a3d7-e311-bccd-0010188dda92',
            "Please assign this ticket to the 'application operation' group.  \r\n  \r\n  \r\n  \r\n \r\n  \r\n \r\n   Hi friends, \r\n  \r\n  \r\n  \r\n \r\n  \r\n \r\n   In order to keep my Miami email. Can someone change my status from a former employee to former graduate student?  \r\n  \r\n  \r\n  \r\n \r\n  \r\n \r\n   Thanks. :)  \r\n  \r\n  \r\n  \r\n \r\n   \r\n   \r\n \r\n  -- \r\n  \r\n \r\n   \r\n    \r\n     \r\n      \r\n       \r\n       \r\n         Weiguo Xia \r\n        \r\n       \r\n         Was an Application Developer@ \r\n        IT-Services",
            Carbon::parse("2020-04-07T14:08:15.233Z"),
            'Weiguo Xia',
            'xiaw@miamioh.edu'
        );

    }
    public function sampleAttachmentUploadResponse()
    {
        return '{"ID":"98ffa4ce-add2-43b8-9c39-1f076164e272","AttachmentType":9,"ItemID":19868670,"CreatedUid":"987f6ec0-3a7d-ec11-94f6-0003ff5063cd","CreatedFullName":"Academic Withdrawal","CreatedDate":"2022-03-08T20:01:29.3Z","Name":"test.txt","Size":2,"Uri":"api/attachments/98ffa4ce-add2-43b8-9c39-1f076164e272","ContentUri":"api/attachments/98ffa4ce-add2-43b8-9c39-1f076164e272/content"}';
    }

    protected function simpleAssetResponse(array $overrides = []): array
    {
        return array_merge([
            "ID" => 528369,
            "AppID" => 741,
            "Name" => "Configuration and Authorization Management (CAM)",
            "Attributes" => [],
            "StatusName" => "In Use",
        ], $overrides);
    }

    protected function newHttpClientWithResponses(array $responses): Client
    {
        $mock = new MockHandler($responses);

        $this->container = [];
        $history = Middleware::history($this->container);

        $handler = HandlerStack::create($mock);
        $handler->push($history);

        return new Client(['handler' => $handler]);
    }

    protected function newJsonResponse(array $content, int $status = 200, array $headers = []): Response
    {
        $body = json_encode($content);

        if (empty($headers)) {
            $headers['content-type'] = 'application/json';
            $headers['content-length'] = strlen($body);
        }

        return new Response(
            $status,
            $headers,
            $body
        );
    }
    protected function sampleRelatedArticlesResponse(): string
    {
        return '[ 
         { "ID": 152123, "AppID": 1813, "AppName": "IT", "CategoryID": 25055, "CategoryName": "ACADEM", "Subject": "ACADEM / Client Documentation", "Body": null, "Summary": "", "Status": 3, "Attributes": [], "ReviewDateUtc": "2025-07-01T00:00:00Z", "Order": 1.0, "IsPublished": true, "IsPublic": false, "WhitelistGroups": true, "InheritPermissions": true, "NotifyOwner": true, "RevisionID": 723321, "RevisionNumber": 14, "DraftStatus": null, "CreatedDate": "2023-06-07T16:44:21.39Z", "CreatedUid": "5f5e3720-40d1-e711-a95b-000d3a137856", "CreatedFullName": "Shane Schlichter", "ModifiedDate": "2024-10-25T18:50:02.097Z", "ModifiedUid": "e698363b-7bcb-e511-b52c-000d3a113e84", "ModifiedFullName": "Em Smith", "OwnerUid": null, "OwnerFullName": null, "OwningGroupID": 2014, "OwningGroupName": "LCM: Solution Delivery Managers", "Tags": null, "Attachments": null, "Uri": "api/1813/knowledgebase/152123" },
         { "ID": 140849, "AppID": 1813, "AppName": "IT", "CategoryID": 22688, "CategoryName": "12Twenty: Administrators", "Subject": "12Twenty FSB / Client Documentation", "Body": null, "Summary": "", "Status": 3, "Attributes": [], "ReviewDateUtc": "2025-02-01T00:00:00Z", "Order": 1.0, "IsPublished": true, "IsPublic": false, "WhitelistGroups": true, "InheritPermissions": true, "NotifyOwner": true, "RevisionID": 679616, "RevisionNumber": 27, "DraftStatus": null, "CreatedDate": "2022-01-24T20:57:27.18Z", "CreatedUid": "d99b26ad-b401-e611-80ce-000d3a133f86", "CreatedFullName": "Festus Ojo", "ModifiedDate": "2024-10-24T16:58:00.54Z", "ModifiedUid": "e698363b-7bcb-e511-b52c-000d3a113e84", "ModifiedFullName": "Em Smith", "OwnerUid": null, "OwnerFullName": null, "OwningGroupID": 2014, "OwningGroupName": "LCM: Solution Delivery Managers", "Tags": null, "Attachments": null, "Uri": "api/1813/knowledgebase/140849" }
        ]';
    }
    protected function sampleAssetReportResponse()
    {
        return '{ "Description": "All In-Use Applications", 
    "MaxResults": 10000, 
    "DisplayedColumns": [ { "HeaderText": "ID", "ColumnName": "AssetID", "DataType": 2, "SortColumnExpression": "AssetID", "SortColumnName": "AssetID", "SortDataType": 2, "Aggregate": 0, "Component": 0, "FooterExpression": null }, { "HeaderText": "Name", "ColumnName": "Name", "DataType": 1, "SortColumnExpression": "Name", "SortColumnName": "Name", "SortDataType": 1, "Aggregate": 0, "Component": 0, "FooterExpression": null } ],
        "SortOrder": [ { "ColumnLabel": "Name", "ColumnName": "Name", "IsAscending": true } ],
        "ChartType": "None", "ChartSettings": [],
        "DataRows": [ 
            { "AssetID": 1046542, "Name": "12Twenty" },
            { "AssetID": 1209137, "Name": "1Password - UIT + CEC Staff Use" },
            { "AssetID": 10216, "Name": "Course List" }
        ],
        "ID": 264009, "Name": "Application: All Applications", "CreatedUid": null, "CreatedFullName": null, "CreatedDate": "2024-07-16T18:59:02.047Z", "OwningGroupID": 1350, "OwningGroupName": "IT Process and Planning", "SystemAppName": "TDAnalysis", "PlatformAppID": 0, "PlatformAppName": "None", "ReportSourceID": 6, "ReportSourceName": "Asset", "Uri": "api/reports/264009" } 
    ';
    }


}
