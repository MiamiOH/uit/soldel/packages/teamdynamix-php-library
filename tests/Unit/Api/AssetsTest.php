<?php

namespace Tests\MiamiOH\TeamDynamix\Unit\Api;

use MiamiOH\TeamDynamix\Api\Assets;
use Tests\MiamiOH\TeamDynamix\Unit\TDTestCase;

use function PHPUnit\Framework\exactly;

class AssetsTest extends TDTestCase
{
    public function testShouldGetAnAsset(): void
    {
        $expectedArray = ['ID' => 1, 'Name' => 'asset name'];
        $this->mockTdClient
            ->method('get')
            ->willReturn($this->newJsonResponse($expectedArray));

        $asset = $this->assetApi->getAsset($expectedArray['ID']);
        $this->assertEquals($expectedArray['ID'], $asset->getId());
    }

    public function testCanGetMultipleAssets(): void
    {
        $asset1Data = $this->simpleAssetResponse(['ID' => 1, 'Name' => 'asset name']);
        $expectedArray = [
            'Data' => [
                ['ID' => 10185],
                ['ID' => 66438],
                ['ID' => 10700],
                ['ID' => 431924]
            ],
            'TotalCount' => 4,
            'CurrentPageIndex' => 0,
            'PageSize' => 5
        ];

        $this->mockTdClient
            ->method('post')
            ->willReturn($this->newJsonResponse($expectedArray));

        $this->mockTdClient
            ->method('get')
            ->willReturnOnConsecutiveCalls(
                $this->newJsonResponse($asset1Data),
                $this->newJsonResponse($this->simpleAssetResponse()),
                $this->newJsonResponse($this->simpleAssetResponse()),
                $this->newJsonResponse($this->simpleAssetResponse())
            );

        $ticketsApi = new Assets($this->mockTdClient);
        $results = $ticketsApi->getAssetsBySearchId('123');
        $this->assertEquals($asset1Data['ID'], $results->first()->getId());
        $this->assertCount(4, $results);
    }

    public function testCanGetMultiplePagesOfAssets()
    {
        $asset1Data = $this->simpleAssetResponse(['ID' => 10185]);
        $asset2Data = $this->simpleAssetResponse([]);
        $expectedSearchArray = [
            'Data' => [
                ['ID' => 10185],
                ['ID' => 66438],
            ],
            'TotalCount' => 4,
            'CurrentPageIndex' => 0,
            'PageSize' => 2
        ];
        $expectedSearchArray2 = [
            'Data' => [
                ['ID' => 10700],
                ['ID' => 431924]
            ],
            'TotalCount' => 4,
            'CurrentPageIndex' => 1,
            'PageSize' => 2
        ];

        $this->mockTdClient
            ->method('post')
            ->willReturnOnConsecutiveCalls(
                $this->newJsonResponse($expectedSearchArray),
                $this->newJsonResponse($expectedSearchArray2),
            );

        $this->mockTdClient
            ->method('get')
            ->willReturnOnConsecutiveCalls(
                $this->newJsonResponse($asset1Data),
                $this->newJsonResponse($asset2Data),
                $this->newJsonResponse($asset1Data),
                $this->newJsonResponse($asset1Data)
            );

        $ticketsApi = new Assets($this->mockTdClient);
        $results = $ticketsApi->getAssetsBySearchId('123', 2);
        $this->assertEquals($asset1Data['ID'], $results->first()->getId());
        $this->assertCount(4, $results);

    }
    public function testCanFilteredSearch(): void
    {
        $asset1Data = $this->simpleAssetResponse(['ID' => 66438, 'Name' => 'asset name']);
        $expectedArray = [
            'Data' => [
                ['ID' => 66438],
            ],
            'TotalCount' => 1,
            'CurrentPageIndex' => 0,
            'PageSize' => 1
        ];

        $this->mockTdClient
            ->method('post')
            ->willReturn($this->newJsonResponse($expectedArray));

        $this->mockTdClient
            ->method('get')
            ->willReturn($this->newJsonResponse($asset1Data));

        $ticketsApi = new Assets($this->mockTdClient);
        $results = $ticketsApi->searchForAssets('123', '66438');
        $this->assertEquals('66438', $results->first()->getId());
        $this->assertCount(1, $results);
    }
    public function testWillSearchWithblankSearch(): void
    {
        $asset1Data = $this->simpleAssetResponse(['ID' => 66438, 'Name' => 'asset name']);
        $asset2Data = $this->simpleAssetResponse(['ID' => 431924]);

        $expectedArray = [
            'Data' => [
                ['ID' => 66438],
                ['ID' => 431924]
            ],
            'TotalCount' => 2,
            'CurrentPageIndex' => 0,
            'PageSize' => 2
        ];

        $this->mockTdClient
            ->method('post')
            ->willReturn($this->newJsonResponse($expectedArray));

        $this->mockTdClient
            ->method('get')
            ->willReturnOnConsecutiveCalls(
                $this->newJsonResponse($asset1Data),
                $this->newJsonResponse($asset2Data)
            );

        $assetsApi = new Assets($this->mockTdClient);
        $results = $assetsApi->searchForAssets('123');
        $this->assertEquals('66438', $results->first()->getId());
        $this->assertCount(2, $results);
    }
    public function testCanGetRelatedArticles()
    {
        $expectedResponse = \json_decode($this->sampleRelatedArticlesResponse(), true);

        $this->mockTdClient
            ->method('get')
            ->willReturn($this->newJsonResponse($expectedResponse));

        $assetsApi = new Assets($this->mockTdClient);
        $results = $assetsApi->getRelatedArticles('123');
        $this->assertCount(2, $results);
    }

    public function testCanGetAllAssetsFromAReport()
    {
        $expectedResponse = \json_decode($this->sampleAssetReportResponse(), true);
        $expectedAssetArray = ['ID' => 1, 'Name' => 'asset name'];

        $this->mockTdClient
            ->expects(exactly(4))
            ->method('get')
            ->willReturnOnConsecutiveCalls(
                $this->newJsonResponse($expectedResponse),
                $this->newJsonResponse($expectedAssetArray),
                $this->newJsonResponse($expectedAssetArray),
                $this->newJsonResponse($expectedAssetArray)
            );
        $assetsApi = new Assets($this->mockTdClient);
        $results = $assetsApi->searchReportForAssets('264009');
        $this->assertCount(3, $results);
    }
    public function testCanGetFilteredAssetsFromAReportStartingWithAString()
    {
        $expectedResponse = \json_decode($this->sampleAssetReportResponse(), true);
        $expectedAssetArray = ['ID' => 1, 'Name' => 'asset name'];
        $this->mockTdClient
            ->expects(exactly(2))
            ->method('get')
            ->willReturnOnConsecutiveCalls(
                $this->newJsonResponse($expectedResponse),
                $this->newJsonResponse($expectedAssetArray)
            );
        $assetsApi = new Assets($this->mockTdClient);
        $results = $assetsApi->searchReportForAssets('264009', 'Course');
        $this->assertCount(1, $results);
    }
    public function testCanGetFilteredAssetsFromAReportContainingAString()
    {
        $expectedResponse = \json_decode($this->sampleAssetReportResponse(), true);
        $expectedAssetArray = ['ID' => 1, 'Name' => 'asset name'];
        $this->mockTdClient
            ->expects(exactly(2))
            ->method('get')
            ->willReturnOnConsecutiveCalls(
                $this->newJsonResponse($expectedResponse),
                $this->newJsonResponse($expectedAssetArray)
            );


        $assetsApi = new Assets($this->mockTdClient);
        $results = $assetsApi->searchReportForAssets('264009', 'UIT');
        $this->assertCount(1, $results);
    }
    public function testCanGetNoAssetsFromAReportWhenMotContainingAString()
    {
        $expectedResponse = \json_decode($this->sampleAssetReportResponse(), true);
        $this->mockTdClient
            ->expects($this->once())
            ->method('get')
            ->willReturn($this->newJsonResponse($expectedResponse));

        $assetsApi = new Assets($this->mockTdClient);
        $results = $assetsApi->searchReportForAssets('264009', 'String not there');
        $this->assertCount(0, $results);
    }


}
