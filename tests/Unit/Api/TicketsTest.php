<?php

namespace Tests\MiamiOH\TeamDynamix\Unit\Api;

use Carbon\Carbon;
use MiamiOH\TeamDynamix\Api\Tickets;
use MiamiOH\TeamDynamix\Collections\TicketCollection;
use MiamiOH\TeamDynamix\Exceptions\AddAttachmentException;
use MiamiOH\TeamDynamix\Exceptions\InvalidTicketDataException;
use MiamiOH\TeamDynamix\Exceptions\TicketCreationException;
use MiamiOH\TeamDynamix\Models\TDTicket;
use MiamiOH\TeamDynamix\Models\TDUser;
use Tests\MiamiOH\TeamDynamix\Unit\TDTestCase;
use MiamiOH\TeamDynamix\Collections\AssetCollection;

class TicketsTest extends TDTestCase
{
    public function testShouldGetATicket(): void
    {
        $ticket = $this->sampleTDTicket();

        $expectedArray = \json_decode($this->sampleIssueResponse(), true);

        $this->mockTdClient
            ->method('get')
            ->willReturn($this->newJsonResponse($expectedArray));

        $ticket = $this->ticketsApi->getTicket($expectedArray['ID']);
        $this->assertEquals($expectedArray['ID'], $ticket->getId());
    }

    public function testGetTicketThrowsErrorsIfDataMissing(): void
    {
        $this->expectException(InvalidTicketDataException::class);
        $this->mockTdClient
            ->method('get')
            ->willReturn($this->newJsonResponse(['ID' => -1, 'Message' => 'Failed']));
        $ticket = $this->ticketsApi->getTicket(-1);

    }
    public function testGetTicketsByUser(): void
    {
        $expectedArray = \json_decode($this->sampleIssueResponse(), true);

        $this->mockTdClient
            ->method('post')
            ->willReturn($this->newJsonResponse([$expectedArray]));

        $coll = new TicketCollection();
        $coll->add(TDTicket::createFromArray($expectedArray));

        $user = new TDUser('ef7af242-a3d7-e311-bccd-0010188dda92', 'Test', 'User', 'testuser@miamioh.edu');

        $this->assertEquals($coll, $this->ticketsApi->getTicketsByUser($user));
    }

    public function testCreateTicket(): void
    {
        $ticket = $this->sampleTDTicket();

        $expectedArray = \json_decode($this->sampleIssueResponse(), true);

        $this->mockTdClient
            ->method('post')
            ->willReturn($this->newJsonResponse($expectedArray));

        $expectedArray['CreatedDate'] = Carbon::parse($expectedArray['CreatedDate']);

        $this->assertEquals($ticket, $this->ticketsApi->createTicket(
            $expectedArray
        ));
    }

    public function testCreateTicketWithApiError(): void
    {
        $this->expectException(TicketCreationException::class);
        $this->mockTdClient
            ->method('post')
            ->willReturn($this->newJsonResponse(['ID' => -1, 'Message' => 'Failed']));

        $ticketData = \json_decode($this->sampleIssueResponse(), true);
        $ticketData['ID'] = -1;
        $ticketData['CreatedDate'] = Carbon::parse($ticketData['CreatedDate']);

        $this->ticketsApi->createTicket(
            $ticketData
        );
    }

    public function testCreateTicketInvalidApiResponse(): void
    {
        $this->expectException(TicketCreationException::class);
        $this->mockTdClient
            ->method('post')
            ->willReturn($this->newJsonResponse(['ID' => null, 'Message' => 'Failed']));

        $ticketData = \json_decode($this->sampleIssueResponse(), true);
        $ticketData['ID'] = -1;
        $ticketData['CreatedDate'] = Carbon::parse($ticketData['CreatedDate']);

        $this->ticketsApi->createTicket(
            $ticketData
        );
    }

    public function testAddAttachmentToTDTicket(): void
    {
        $attachmentResponse = json_decode($this->sampleAttachmentUploadResponse(), true);

        $this->mockTdClient
            ->method('post')
            ->willReturn($this->newJsonResponse($attachmentResponse));

        $ticketsApi = new Tickets($this->mockTdClient);

        $multipart['multipart'] = [
            'name' => 'test.txt',
            'contents'=> 'test',
            'filename'=> 'test.txt',
        ];

        $this->assertEquals($attachmentResponse, $ticketsApi->addAttachmentToTicket($multipart, 123));
    }

    public function testAddAttachmentToTDTicketWithApiError(): void
    {
        $this->expectException(AddAttachmentException::class);

        $this->mockTdClient
            ->method('post')
            ->willReturn($this->newJsonResponse(['ID' => -1, 'Message' => 'Failed']));
        $ticketsApi = new Tickets($this->mockTdClient);

        $multipart['multipart'] = [
            'name' => 'test.txt',
            'contents'=> 'test',
            'filename'=> 'test.txt',
        ];

        $ticketsApi->addAttachmentToTicket($multipart, 123);
    }
    public function testGetTicketWithNoAssets(): void
    {
        $ticket = $this->sampleTDTicket();

        $expectedArray = \json_decode($this->sampleIssueResponse(), true);
        $assetsArray = [];

        $this->mockTdClient
            ->method('get')
            ->willReturnOnConsecutiveCalls($this->newJsonResponse($expectedArray), $this->newJsonResponse($assetsArray));

        $ticket = $this->ticketsApi->getTicketwithAssets($expectedArray['ID']);
        $this->assertEquals($expectedArray['ID'], $ticket->getId());
        $assets = $ticket->getAssets();
        $this->assertEmpty($assets);
        $this->assertInstanceOf(AssetCollection::class, $ticket->getAssets());
    }
    public function testGetTicketWithManyAssets(): void
    {
        $ticket = $this->sampleTDTicket();

        $expectedArray = \json_decode($this->sampleIssueResponse(), true);
        $assetsArray = \json_decode($this->sampleTicketWithMultipleAssets(), true);

        $this->mockTdClient
            ->method('get')
            ->willReturnOnConsecutiveCalls($this->newJsonResponse($expectedArray), $this->newJsonResponse($assetsArray));

        $ticket = $this->ticketsApi->getTicketwithAssets($expectedArray['ID']);
        $this->assertEquals($expectedArray['ID'], $ticket->getId());
        $this->assertInstanceOf(AssetCollection::class, $ticket->getAssets());
    }
    public function testGetTicketWithOneAsset(): void
    {
        $ticket = $this->sampleTDTicket();

        $expectedArray = \json_decode($this->sampleIssueResponse(), true);
        $assetsArray = \json_decode($this->sampleTicketWithSingleAsset(), true);

        $this->mockTdClient
            ->method('get')
            ->willReturnOnConsecutiveCalls($this->newJsonResponse($expectedArray), $this->newJsonResponse($assetsArray));

        $ticket = $this->ticketsApi->getTicketwithAssets($expectedArray['ID']);
        $this->assertEquals($expectedArray['ID'], $ticket->getId());
        $assets = $ticket->getAssets();
        $this->assertEquals($assetsArray[0]['ID'], $assets->first()->getId());
        $this->assertInstanceOf(AssetCollection::class, $ticket->getAssets());
    }
}
