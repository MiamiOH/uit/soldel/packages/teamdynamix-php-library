<?php

namespace Tests\MiamiOH\TeamDynamix\Unit\Api;

use MiamiOH\TeamDynamix\Api\Users;
use MiamiOH\TeamDynamix\Collections\UserCollection;
use MiamiOH\TeamDynamix\Models\TDUser;
use MiamiOH\TeamDynamix\TDClient;
use Tests\MiamiOH\TeamDynamix\Unit\TDTestCase;

class UsersTest extends TDTestCase
{
    public function testSearchUsers(): void
    {
        $expectedArray = \json_decode($this->sampleUserResponse(), true);

        $this->mockTdClient
            ->method('get')
            ->willReturn($this->newJsonResponse([$expectedArray]));
        $usersApi = new Users($this->mockTdClient);

        $coll = new UserCollection();
        $coll->add(TDUser::createFromArray($expectedArray));

        $this->assertEquals($coll, $usersApi->searchUsers("test", 1));
    }
}
