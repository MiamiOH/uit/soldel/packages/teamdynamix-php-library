<?php

namespace Tests\MiamiOH\TeamDynamix\Unit;

use MiamiOH\TeamDynamix\TDClient;
use Carbon\Carbon;
use GuzzleHttp\Exception\ClientException;
use GuzzleHttp\Psr7\Request;
use GuzzleHttp\Psr7\Response;
use Tests\MiamiOH\TeamDynamix\Unit\TDTestCase;

class TDClientTest extends TDTestCase
{
    protected $container = [];

    /**
     * @var TDClient
     */
    private $TDClient;


    public function testCanCreateTDClient()
    {
        $client = $this->newHttpClientWithResponses([]);
        $this->TDClient = new TDClient('123', $client, 'user', 'password');
        $this->assertInstanceOf(TDClient::class, $this->TDClient);
    }

    public function testCanCreateGetRequest()
    {
        $token = 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9';
        $data = json_decode($this->fullSampleAssetResponse(), true);
        $client = $this->newHttpClientWithResponses([
            $this->newJsonResponse([$token]),
            $this->newJsonResponse($data)
        ]);
        $this->TDClient = new TDClient('123', $client, 'user', 'password');
        $uriPath = 'assets/528369';

        $response = $this->TDClient->get($uriPath);
        $this->assertCount(2, $this->container);
        /** @var Request $resourceRequest */
        $resourceRequest = $this->container[1]['request'];
        $this->assertEquals('GET', $resourceRequest->getMethod());
        $this->assertEquals(200, $response->getStatusCode());
    }

    public function testCanCreatePostRequestWithFormParams()
    {
        $token = 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9';
        $data = json_decode($this->fullSampleAssetResponse(), true);
        $client = $this->newHttpClientWithResponses([
            $this->newJsonResponse([$token]),
            $this->newJsonResponse($data)
        ]);
        $this->TDClient = new TDClient('123', $client, 'user', 'password');
        $uriPath = 'assets/528369';
        $body = [
            'form_params' => []
        ];
        $header = [
            'Content-Type' => 'application/x-www-form-urlencoded'
        ];

        $response = $this->TDClient->post($uriPath, $body, $header);
        $this->assertCount(2, $this->container);
        /** @var Request $resourceRequest */
        $resourceRequest = $this->container[1]['request'];
        $this->assertEquals('POST', $resourceRequest->getMethod());
        $this->assertEquals(200, $response->getStatusCode());

    }

    public function testCanCreatePostRequestWithJson()
    {
        $token = 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9';

        $data = json_decode($this->sampleIssueResponse(), true);
        $client = $this->newHttpClientWithResponses([
            $this->newJsonResponse([$token]),
            $this->newJsonResponse([$data])
        ]);
        $this->TDClient = new TDClient('123', $client, 'user', 'password');
        $uriPath = 'tickets/search';
        $body = [
            'body' => '[]'
        ];
        $headers = [
            'Content-Type' => 'application/json'
        ];

        $response = $this->TDClient->post($uriPath, $body, $headers);
        $this->assertCount(2, $this->container);

        $resourceRequest = $this->container[1]['request'];
        $this->assertEquals('POST', $resourceRequest->getMethod());

        $this->assertEquals(200, $response->getStatusCode());
    }

    public function testCanReturnToken()
    {
        $token = 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9';
        $headers = [];
        $headers['content-type'] = 'application/json';
        $headers['content-length'] = strlen($token);

        $client = $this->newHttpClientWithResponses([
            new Response(
                200,
                $headers,
                $token
            )
        ]);
        $this->TDClient = new TDClient('123', $client, 'user', 'password');
        $this->assertEquals($token, $this->TDClient->getToken());
    }

    public function testCanHandle429Error()
    {
        $token = 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9';
        $headers['content-type'] = 'application/json';
        $headers['content-length'] = strlen($token);
        $headers['X-RateLimit-Limit'] = 60;
        $headers['X-RateLimit-Remaining'] = 0;
        $mockedCurrentTime = Carbon::create(2020, 1, 1);
        Carbon::setTestNow($mockedCurrentTime);
        $headers['X-RateLimit-Reset'] = (string)Carbon::now()->addSeconds(1);

        $data = json_decode($this->fullSampleAssetResponse(), true);
        $client = $this->newHttpClientWithResponses([
            $this->newJsonResponse([$token]),
            $this->newJsonResponse(['This can only be requested 60 times every 60 seconds.'], 429, $headers),
            $this->newJsonResponse($data),
        ]);
        $uriPath = 'assets/528369';
        $this->TDClient = new TDClient('123', $client, 'user', 'password');
        $response = $this->TDClient->get($uriPath);
        $this->assertCount(3, $this->container);
        /** @var Request $resourceRequest */
        $resourceRequest = $this->container[1]['request'];
        $this->assertEquals('GET', $resourceRequest->getMethod());
        $this->assertEquals(200, $response->getStatusCode());
    }

    public function testCanCatchErrorsAndDisplayMessages()
    {
        $token = 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9';
        $message = 'You can\'t do that.';
        $data = json_decode($this->fullSampleAssetResponse(), true);
        $client = $this->newHttpClientWithResponses([
            $this->newJsonResponse([$token]),
            $this->newJsonResponse([$message], 401),
        ]);
        $uriPath = 'assets/528369';
        $this->TDClient = new TDClient('123', $client, 'user', 'password');
        $this->expectException(ClientException::class);
        $this->TDClient->get($uriPath);

    }

    public function testCanGetAppId(): void
    {
        $this->TDClient = new TDClient('123', $this->newHttpClientWithResponses([]), 'user', 'password');
        $this->assertEquals('123', $this->TDClient->getAppId());
    }

    public function testCanCreateMultiPartPostRequest()
    {
        $token = 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9';
        $data = json_decode($this->sampleAttachmentUploadResponse(), true);

        $client = $this->newHttpClientWithResponses([
            $this->newJsonResponse([$token]),
            $this->newJsonResponse([$data])
        ]);
        $this->TDClient = new TDClient('123', $client, 'user', 'password');
        $uriPath = 'tickets/123/attachments';

        $multipart['multipart'][] = [
            'name' => 'test.txt',
            'contents'=> 'test',
            'filename'=> 'test.txt',
        ];

        $response = $this->TDClient->post($uriPath, $multipart, []);
        $this->assertCount(2, $this->container);

        $resourceRequest = $this->container[1]['request'];
        $this->assertEquals('POST', $resourceRequest->getMethod());
        $this->assertEquals(200, $response->getStatusCode());
    }

    public function testCanPostDontNotifyRequestor(): void
    {
        $token = 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9';

        $data = json_decode($this->sampleIssueResponse(), true);
        $client = $this->newHttpClientWithResponses([
            $this->newJsonResponse([$token]),
            $this->newJsonResponse([$data])
        ]);
        $this->TDClient = new TDClient('123', $client, 'user', 'password');
        $uriPath = 'tickets/search';
        $body = [
            'body' => '[]'
        ];
        $headers = [
            'Content-Type' => 'application/json'
        ];
        $this->TDClient->setNotifyRequestor(false);

        $response = $this->TDClient->post($uriPath, $body, $headers);

        $resourceRequest = $this->container[1]['request'];
        $query = $resourceRequest->getUri()->getQuery();

        $this->assertEquals("NotifyRequestor=false", $query);
    }
}
