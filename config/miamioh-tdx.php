<?php

return [
    'host' => env('TDX_HOST', 'https://api.teamdynamix.com/TDWebApi/api/'),
    'appId' => env('TDX_APPID', ''),
    'username' => env('TDX_USER', ''),
    'password' => env('TDX_PASS', ''),
    'articleBaseUrl' => env('TDX_ARTICLES_BASE_URL', 'https://miamioh.teamdynamix.com/TDClient/'),
];
